require=(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
if (typeof Object.create === 'function') {
  // implementation from standard node.js 'util' module
  module.exports = function inherits(ctor, superCtor) {
    ctor.super_ = superCtor
    ctor.prototype = Object.create(superCtor.prototype, {
      constructor: {
        value: ctor,
        enumerable: false,
        writable: true,
        configurable: true
      }
    });
  };
} else {
  // old school shim for old browsers
  module.exports = function inherits(ctor, superCtor) {
    ctor.super_ = superCtor
    var TempCtor = function () {}
    TempCtor.prototype = superCtor.prototype
    ctor.prototype = new TempCtor()
    ctor.prototype.constructor = ctor
  }
}

},{}],2:[function(require,module,exports){
(function (process){
// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.

// resolves . and .. elements in a path array with directory names there
// must be no slashes, empty elements, or device names (c:\) in the array
// (so also no leading and trailing slashes - it does not distinguish
// relative and absolute paths)
function normalizeArray(parts, allowAboveRoot) {
  // if the path tries to go above the root, `up` ends up > 0
  var up = 0;
  for (var i = parts.length - 1; i >= 0; i--) {
    var last = parts[i];
    if (last === '.') {
      parts.splice(i, 1);
    } else if (last === '..') {
      parts.splice(i, 1);
      up++;
    } else if (up) {
      parts.splice(i, 1);
      up--;
    }
  }

  // if the path is allowed to go above the root, restore leading ..s
  if (allowAboveRoot) {
    for (; up--; up) {
      parts.unshift('..');
    }
  }

  return parts;
}

// Split a filename into [root, dir, basename, ext], unix version
// 'root' is just a slash, or nothing.
var splitPathRe =
    /^(\/?|)([\s\S]*?)((?:\.{1,2}|[^\/]+?|)(\.[^.\/]*|))(?:[\/]*)$/;
var splitPath = function(filename) {
  return splitPathRe.exec(filename).slice(1);
};

// path.resolve([from ...], to)
// posix version
exports.resolve = function() {
  var resolvedPath = '',
      resolvedAbsolute = false;

  for (var i = arguments.length - 1; i >= -1 && !resolvedAbsolute; i--) {
    var path = (i >= 0) ? arguments[i] : process.cwd();

    // Skip empty and invalid entries
    if (typeof path !== 'string') {
      throw new TypeError('Arguments to path.resolve must be strings');
    } else if (!path) {
      continue;
    }

    resolvedPath = path + '/' + resolvedPath;
    resolvedAbsolute = path.charAt(0) === '/';
  }

  // At this point the path should be resolved to a full absolute path, but
  // handle relative paths to be safe (might happen when process.cwd() fails)

  // Normalize the path
  resolvedPath = normalizeArray(filter(resolvedPath.split('/'), function(p) {
    return !!p;
  }), !resolvedAbsolute).join('/');

  return ((resolvedAbsolute ? '/' : '') + resolvedPath) || '.';
};

// path.normalize(path)
// posix version
exports.normalize = function(path) {
  var isAbsolute = exports.isAbsolute(path),
      trailingSlash = substr(path, -1) === '/';

  // Normalize the path
  path = normalizeArray(filter(path.split('/'), function(p) {
    return !!p;
  }), !isAbsolute).join('/');

  if (!path && !isAbsolute) {
    path = '.';
  }
  if (path && trailingSlash) {
    path += '/';
  }

  return (isAbsolute ? '/' : '') + path;
};

// posix version
exports.isAbsolute = function(path) {
  return path.charAt(0) === '/';
};

// posix version
exports.join = function() {
  var paths = Array.prototype.slice.call(arguments, 0);
  return exports.normalize(filter(paths, function(p, index) {
    if (typeof p !== 'string') {
      throw new TypeError('Arguments to path.join must be strings');
    }
    return p;
  }).join('/'));
};


// path.relative(from, to)
// posix version
exports.relative = function(from, to) {
  from = exports.resolve(from).substr(1);
  to = exports.resolve(to).substr(1);

  function trim(arr) {
    var start = 0;
    for (; start < arr.length; start++) {
      if (arr[start] !== '') break;
    }

    var end = arr.length - 1;
    for (; end >= 0; end--) {
      if (arr[end] !== '') break;
    }

    if (start > end) return [];
    return arr.slice(start, end - start + 1);
  }

  var fromParts = trim(from.split('/'));
  var toParts = trim(to.split('/'));

  var length = Math.min(fromParts.length, toParts.length);
  var samePartsLength = length;
  for (var i = 0; i < length; i++) {
    if (fromParts[i] !== toParts[i]) {
      samePartsLength = i;
      break;
    }
  }

  var outputParts = [];
  for (var i = samePartsLength; i < fromParts.length; i++) {
    outputParts.push('..');
  }

  outputParts = outputParts.concat(toParts.slice(samePartsLength));

  return outputParts.join('/');
};

exports.sep = '/';
exports.delimiter = ':';

exports.dirname = function(path) {
  var result = splitPath(path),
      root = result[0],
      dir = result[1];

  if (!root && !dir) {
    // No dirname whatsoever
    return '.';
  }

  if (dir) {
    // It has a dirname, strip trailing slash
    dir = dir.substr(0, dir.length - 1);
  }

  return root + dir;
};


exports.basename = function(path, ext) {
  var f = splitPath(path)[2];
  // TODO: make this comparison case-insensitive on windows?
  if (ext && f.substr(-1 * ext.length) === ext) {
    f = f.substr(0, f.length - ext.length);
  }
  return f;
};


exports.extname = function(path) {
  return splitPath(path)[3];
};

function filter (xs, f) {
    if (xs.filter) return xs.filter(f);
    var res = [];
    for (var i = 0; i < xs.length; i++) {
        if (f(xs[i], i, xs)) res.push(xs[i]);
    }
    return res;
}

// String.prototype.substr - negative index don't work in IE8
var substr = 'ab'.substr(-1) === 'b'
    ? function (str, start, len) { return str.substr(start, len) }
    : function (str, start, len) {
        if (start < 0) start = str.length + start;
        return str.substr(start, len);
    }
;

}).call(this,require('_process'))
},{"_process":3}],3:[function(require,module,exports){
// shim for using process in browser

var process = module.exports = {};
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = setTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            currentQueue[queueIndex].run();
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    clearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        setTimeout(drainQueue, 0);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

// TODO(shtylman)
process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };

},{}],4:[function(require,module,exports){
module.exports = function isBuffer(arg) {
  return arg && typeof arg === 'object'
    && typeof arg.copy === 'function'
    && typeof arg.fill === 'function'
    && typeof arg.readUInt8 === 'function';
}
},{}],5:[function(require,module,exports){
(function (process,global){
// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.

var formatRegExp = /%[sdj%]/g;
exports.format = function(f) {
  if (!isString(f)) {
    var objects = [];
    for (var i = 0; i < arguments.length; i++) {
      objects.push(inspect(arguments[i]));
    }
    return objects.join(' ');
  }

  var i = 1;
  var args = arguments;
  var len = args.length;
  var str = String(f).replace(formatRegExp, function(x) {
    if (x === '%%') return '%';
    if (i >= len) return x;
    switch (x) {
      case '%s': return String(args[i++]);
      case '%d': return Number(args[i++]);
      case '%j':
        try {
          return JSON.stringify(args[i++]);
        } catch (_) {
          return '[Circular]';
        }
      default:
        return x;
    }
  });
  for (var x = args[i]; i < len; x = args[++i]) {
    if (isNull(x) || !isObject(x)) {
      str += ' ' + x;
    } else {
      str += ' ' + inspect(x);
    }
  }
  return str;
};


// Mark that a method should not be used.
// Returns a modified function which warns once by default.
// If --no-deprecation is set, then it is a no-op.
exports.deprecate = function(fn, msg) {
  // Allow for deprecating things in the process of starting up.
  if (isUndefined(global.process)) {
    return function() {
      return exports.deprecate(fn, msg).apply(this, arguments);
    };
  }

  if (process.noDeprecation === true) {
    return fn;
  }

  var warned = false;
  function deprecated() {
    if (!warned) {
      if (process.throwDeprecation) {
        throw new Error(msg);
      } else if (process.traceDeprecation) {
        console.trace(msg);
      } else {
        console.error(msg);
      }
      warned = true;
    }
    return fn.apply(this, arguments);
  }

  return deprecated;
};


var debugs = {};
var debugEnviron;
exports.debuglog = function(set) {
  if (isUndefined(debugEnviron))
    debugEnviron = process.env.NODE_DEBUG || '';
  set = set.toUpperCase();
  if (!debugs[set]) {
    if (new RegExp('\\b' + set + '\\b', 'i').test(debugEnviron)) {
      var pid = process.pid;
      debugs[set] = function() {
        var msg = exports.format.apply(exports, arguments);
        console.error('%s %d: %s', set, pid, msg);
      };
    } else {
      debugs[set] = function() {};
    }
  }
  return debugs[set];
};


/**
 * Echos the value of a value. Trys to print the value out
 * in the best way possible given the different types.
 *
 * @param {Object} obj The object to print out.
 * @param {Object} opts Optional options object that alters the output.
 */
/* legacy: obj, showHidden, depth, colors*/
function inspect(obj, opts) {
  // default options
  var ctx = {
    seen: [],
    stylize: stylizeNoColor
  };
  // legacy...
  if (arguments.length >= 3) ctx.depth = arguments[2];
  if (arguments.length >= 4) ctx.colors = arguments[3];
  if (isBoolean(opts)) {
    // legacy...
    ctx.showHidden = opts;
  } else if (opts) {
    // got an "options" object
    exports._extend(ctx, opts);
  }
  // set default options
  if (isUndefined(ctx.showHidden)) ctx.showHidden = false;
  if (isUndefined(ctx.depth)) ctx.depth = 2;
  if (isUndefined(ctx.colors)) ctx.colors = false;
  if (isUndefined(ctx.customInspect)) ctx.customInspect = true;
  if (ctx.colors) ctx.stylize = stylizeWithColor;
  return formatValue(ctx, obj, ctx.depth);
}
exports.inspect = inspect;


// http://en.wikipedia.org/wiki/ANSI_escape_code#graphics
inspect.colors = {
  'bold' : [1, 22],
  'italic' : [3, 23],
  'underline' : [4, 24],
  'inverse' : [7, 27],
  'white' : [37, 39],
  'grey' : [90, 39],
  'black' : [30, 39],
  'blue' : [34, 39],
  'cyan' : [36, 39],
  'green' : [32, 39],
  'magenta' : [35, 39],
  'red' : [31, 39],
  'yellow' : [33, 39]
};

// Don't use 'blue' not visible on cmd.exe
inspect.styles = {
  'special': 'cyan',
  'number': 'yellow',
  'boolean': 'yellow',
  'undefined': 'grey',
  'null': 'bold',
  'string': 'green',
  'date': 'magenta',
  // "name": intentionally not styling
  'regexp': 'red'
};


function stylizeWithColor(str, styleType) {
  var style = inspect.styles[styleType];

  if (style) {
    return '\u001b[' + inspect.colors[style][0] + 'm' + str +
           '\u001b[' + inspect.colors[style][1] + 'm';
  } else {
    return str;
  }
}


function stylizeNoColor(str, styleType) {
  return str;
}


function arrayToHash(array) {
  var hash = {};

  array.forEach(function(val, idx) {
    hash[val] = true;
  });

  return hash;
}


function formatValue(ctx, value, recurseTimes) {
  // Provide a hook for user-specified inspect functions.
  // Check that value is an object with an inspect function on it
  if (ctx.customInspect &&
      value &&
      isFunction(value.inspect) &&
      // Filter out the util module, it's inspect function is special
      value.inspect !== exports.inspect &&
      // Also filter out any prototype objects using the circular check.
      !(value.constructor && value.constructor.prototype === value)) {
    var ret = value.inspect(recurseTimes, ctx);
    if (!isString(ret)) {
      ret = formatValue(ctx, ret, recurseTimes);
    }
    return ret;
  }

  // Primitive types cannot have properties
  var primitive = formatPrimitive(ctx, value);
  if (primitive) {
    return primitive;
  }

  // Look up the keys of the object.
  var keys = Object.keys(value);
  var visibleKeys = arrayToHash(keys);

  if (ctx.showHidden) {
    keys = Object.getOwnPropertyNames(value);
  }

  // IE doesn't make error fields non-enumerable
  // http://msdn.microsoft.com/en-us/library/ie/dww52sbt(v=vs.94).aspx
  if (isError(value)
      && (keys.indexOf('message') >= 0 || keys.indexOf('description') >= 0)) {
    return formatError(value);
  }

  // Some type of object without properties can be shortcutted.
  if (keys.length === 0) {
    if (isFunction(value)) {
      var name = value.name ? ': ' + value.name : '';
      return ctx.stylize('[Function' + name + ']', 'special');
    }
    if (isRegExp(value)) {
      return ctx.stylize(RegExp.prototype.toString.call(value), 'regexp');
    }
    if (isDate(value)) {
      return ctx.stylize(Date.prototype.toString.call(value), 'date');
    }
    if (isError(value)) {
      return formatError(value);
    }
  }

  var base = '', array = false, braces = ['{', '}'];

  // Make Array say that they are Array
  if (isArray(value)) {
    array = true;
    braces = ['[', ']'];
  }

  // Make functions say that they are functions
  if (isFunction(value)) {
    var n = value.name ? ': ' + value.name : '';
    base = ' [Function' + n + ']';
  }

  // Make RegExps say that they are RegExps
  if (isRegExp(value)) {
    base = ' ' + RegExp.prototype.toString.call(value);
  }

  // Make dates with properties first say the date
  if (isDate(value)) {
    base = ' ' + Date.prototype.toUTCString.call(value);
  }

  // Make error with message first say the error
  if (isError(value)) {
    base = ' ' + formatError(value);
  }

  if (keys.length === 0 && (!array || value.length == 0)) {
    return braces[0] + base + braces[1];
  }

  if (recurseTimes < 0) {
    if (isRegExp(value)) {
      return ctx.stylize(RegExp.prototype.toString.call(value), 'regexp');
    } else {
      return ctx.stylize('[Object]', 'special');
    }
  }

  ctx.seen.push(value);

  var output;
  if (array) {
    output = formatArray(ctx, value, recurseTimes, visibleKeys, keys);
  } else {
    output = keys.map(function(key) {
      return formatProperty(ctx, value, recurseTimes, visibleKeys, key, array);
    });
  }

  ctx.seen.pop();

  return reduceToSingleString(output, base, braces);
}


function formatPrimitive(ctx, value) {
  if (isUndefined(value))
    return ctx.stylize('undefined', 'undefined');
  if (isString(value)) {
    var simple = '\'' + JSON.stringify(value).replace(/^"|"$/g, '')
                                             .replace(/'/g, "\\'")
                                             .replace(/\\"/g, '"') + '\'';
    return ctx.stylize(simple, 'string');
  }
  if (isNumber(value))
    return ctx.stylize('' + value, 'number');
  if (isBoolean(value))
    return ctx.stylize('' + value, 'boolean');
  // For some reason typeof null is "object", so special case here.
  if (isNull(value))
    return ctx.stylize('null', 'null');
}


function formatError(value) {
  return '[' + Error.prototype.toString.call(value) + ']';
}


function formatArray(ctx, value, recurseTimes, visibleKeys, keys) {
  var output = [];
  for (var i = 0, l = value.length; i < l; ++i) {
    if (hasOwnProperty(value, String(i))) {
      output.push(formatProperty(ctx, value, recurseTimes, visibleKeys,
          String(i), true));
    } else {
      output.push('');
    }
  }
  keys.forEach(function(key) {
    if (!key.match(/^\d+$/)) {
      output.push(formatProperty(ctx, value, recurseTimes, visibleKeys,
          key, true));
    }
  });
  return output;
}


function formatProperty(ctx, value, recurseTimes, visibleKeys, key, array) {
  var name, str, desc;
  desc = Object.getOwnPropertyDescriptor(value, key) || { value: value[key] };
  if (desc.get) {
    if (desc.set) {
      str = ctx.stylize('[Getter/Setter]', 'special');
    } else {
      str = ctx.stylize('[Getter]', 'special');
    }
  } else {
    if (desc.set) {
      str = ctx.stylize('[Setter]', 'special');
    }
  }
  if (!hasOwnProperty(visibleKeys, key)) {
    name = '[' + key + ']';
  }
  if (!str) {
    if (ctx.seen.indexOf(desc.value) < 0) {
      if (isNull(recurseTimes)) {
        str = formatValue(ctx, desc.value, null);
      } else {
        str = formatValue(ctx, desc.value, recurseTimes - 1);
      }
      if (str.indexOf('\n') > -1) {
        if (array) {
          str = str.split('\n').map(function(line) {
            return '  ' + line;
          }).join('\n').substr(2);
        } else {
          str = '\n' + str.split('\n').map(function(line) {
            return '   ' + line;
          }).join('\n');
        }
      }
    } else {
      str = ctx.stylize('[Circular]', 'special');
    }
  }
  if (isUndefined(name)) {
    if (array && key.match(/^\d+$/)) {
      return str;
    }
    name = JSON.stringify('' + key);
    if (name.match(/^"([a-zA-Z_][a-zA-Z_0-9]*)"$/)) {
      name = name.substr(1, name.length - 2);
      name = ctx.stylize(name, 'name');
    } else {
      name = name.replace(/'/g, "\\'")
                 .replace(/\\"/g, '"')
                 .replace(/(^"|"$)/g, "'");
      name = ctx.stylize(name, 'string');
    }
  }

  return name + ': ' + str;
}


function reduceToSingleString(output, base, braces) {
  var numLinesEst = 0;
  var length = output.reduce(function(prev, cur) {
    numLinesEst++;
    if (cur.indexOf('\n') >= 0) numLinesEst++;
    return prev + cur.replace(/\u001b\[\d\d?m/g, '').length + 1;
  }, 0);

  if (length > 60) {
    return braces[0] +
           (base === '' ? '' : base + '\n ') +
           ' ' +
           output.join(',\n  ') +
           ' ' +
           braces[1];
  }

  return braces[0] + base + ' ' + output.join(', ') + ' ' + braces[1];
}


// NOTE: These type checking functions intentionally don't use `instanceof`
// because it is fragile and can be easily faked with `Object.create()`.
function isArray(ar) {
  return Array.isArray(ar);
}
exports.isArray = isArray;

function isBoolean(arg) {
  return typeof arg === 'boolean';
}
exports.isBoolean = isBoolean;

function isNull(arg) {
  return arg === null;
}
exports.isNull = isNull;

function isNullOrUndefined(arg) {
  return arg == null;
}
exports.isNullOrUndefined = isNullOrUndefined;

function isNumber(arg) {
  return typeof arg === 'number';
}
exports.isNumber = isNumber;

function isString(arg) {
  return typeof arg === 'string';
}
exports.isString = isString;

function isSymbol(arg) {
  return typeof arg === 'symbol';
}
exports.isSymbol = isSymbol;

function isUndefined(arg) {
  return arg === void 0;
}
exports.isUndefined = isUndefined;

function isRegExp(re) {
  return isObject(re) && objectToString(re) === '[object RegExp]';
}
exports.isRegExp = isRegExp;

function isObject(arg) {
  return typeof arg === 'object' && arg !== null;
}
exports.isObject = isObject;

function isDate(d) {
  return isObject(d) && objectToString(d) === '[object Date]';
}
exports.isDate = isDate;

function isError(e) {
  return isObject(e) &&
      (objectToString(e) === '[object Error]' || e instanceof Error);
}
exports.isError = isError;

function isFunction(arg) {
  return typeof arg === 'function';
}
exports.isFunction = isFunction;

function isPrimitive(arg) {
  return arg === null ||
         typeof arg === 'boolean' ||
         typeof arg === 'number' ||
         typeof arg === 'string' ||
         typeof arg === 'symbol' ||  // ES6 symbol
         typeof arg === 'undefined';
}
exports.isPrimitive = isPrimitive;

exports.isBuffer = require('./support/isBuffer');

function objectToString(o) {
  return Object.prototype.toString.call(o);
}


function pad(n) {
  return n < 10 ? '0' + n.toString(10) : n.toString(10);
}


var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep',
              'Oct', 'Nov', 'Dec'];

// 26 Feb 16:19:34
function timestamp() {
  var d = new Date();
  var time = [pad(d.getHours()),
              pad(d.getMinutes()),
              pad(d.getSeconds())].join(':');
  return [d.getDate(), months[d.getMonth()], time].join(' ');
}


// log is just a thin wrapper to console.log that prepends a timestamp
exports.log = function() {
  console.log('%s - %s', timestamp(), exports.format.apply(exports, arguments));
};


/**
 * Inherit the prototype methods from one constructor into another.
 *
 * The Function.prototype.inherits from lang.js rewritten as a standalone
 * function (not on Function.prototype). NOTE: If this file is to be loaded
 * during bootstrapping this function needs to be rewritten using some native
 * functions as prototype setup using normal JavaScript does not work as
 * expected during bootstrapping (see mirror.js in r114903).
 *
 * @param {function} ctor Constructor function which needs to inherit the
 *     prototype.
 * @param {function} superCtor Constructor function to inherit prototype from.
 */
exports.inherits = require('inherits');

exports._extend = function(origin, add) {
  // Don't do anything if add isn't an object
  if (!add || !isObject(add)) return origin;

  var keys = Object.keys(add);
  var i = keys.length;
  while (i--) {
    origin[keys[i]] = add[keys[i]];
  }
  return origin;
};

function hasOwnProperty(obj, prop) {
  return Object.prototype.hasOwnProperty.call(obj, prop);
}

}).call(this,require('_process'),typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./support/isBuffer":4,"_process":3,"inherits":1}],6:[function(require,module,exports){
/**
 * @fileoverview Prevent missing displayName in a React component definition
 * @author Yannick Croissant
 */
'use strict';

var componentUtil = require('../util/component');
var ComponentList = componentUtil.List;

// ------------------------------------------------------------------------------
// Rule Definition
// ------------------------------------------------------------------------------

module.exports = function(context) {

  var config = context.options[0] || {};
  var acceptTranspilerName = config.acceptTranspilerName || false;

  var componentList = new ComponentList();

  var MISSING_MESSAGE = 'Component definition is missing display name';
  var MISSING_MESSAGE_NAMED_COMP = '{{component}} component definition is missing display name';

  /**
   * Checks if the component must be validated
   * @param {Object} component The component to process
   * @returns {Boolean} True if the component must be validated, false if not.
   */
  function mustBeValidated(component) {
    return (
      component &&
      component.isReactComponent &&
      !component.hasDisplayName
    );
  }

  /**
   * Checks if we are declaring a display name
   * @param {ASTNode} node The AST node being checked.
   * @returns {Boolean} True if we are declaring a display name, false if not.
   */
  function isDisplayNameDeclaration(node) {

    // Special case for class properties
    // (babel-eslint does not expose property name so we have to rely on tokens)
    if (node.type === 'ClassProperty') {
      var tokens = context.getFirstTokens(node, 2);
      if (
        tokens[0].value === 'displayName' ||
        (tokens[1] && tokens[1].value === 'displayName')
      ) {
        return true;
      }
      return false;
    }

    return Boolean(
      node &&
      node.name === 'displayName'
    );
  }

  /**
   * Mark a prop type as declared
   * @param {ASTNode} node The AST node being checked.
   */
  function markDisplayNameAsDeclared(node) {
    componentList.set(context, node, {
      hasDisplayName: true
    });
  }

  /**
   * Reports missing display name for a given component
   * @param {Object} component The component to process
   */
  function reportMissingDisplayName(component) {
    context.report(
      component.node,
      component.name === componentUtil.DEFAULT_COMPONENT_NAME ? MISSING_MESSAGE : MISSING_MESSAGE_NAMED_COMP, {
        component: component.name
      }
    );
  }

  /**
   * Checks if the component have a name set by the transpiler
   * @param {ASTNode} node The AST node being checked.
   * @returns {Boolean} True ifcomponent have a name, false if not.
   */
  function hasTranspilerName(node) {
    var namedAssignment = (
      node.type === 'ObjectExpression' &&
      node.parent &&
      node.parent.parent &&
      node.parent.parent.type === 'AssignmentExpression' && (
        !node.parent.parent.left.object ||
        node.parent.parent.left.object.name !== 'module' ||
        node.parent.parent.left.property.name !== 'exports'
      )
    );
    var namedDeclaration = (
        node.type === 'ObjectExpression' &&
        node.parent &&
        node.parent.parent &&
        node.parent.parent.type === 'VariableDeclarator'
    );
    var namedClass = (
      node.type === 'ClassDeclaration' &&
      node.id && node.id.name
    );

    if (namedAssignment || namedDeclaration || namedClass) {
      return true;
    }
    return false;
  }

  // --------------------------------------------------------------------------
  // Public
  // --------------------------------------------------------------------------

  return {

    ClassProperty: function(node) {
      if (!isDisplayNameDeclaration(node)) {
        return;
      }

      markDisplayNameAsDeclared(node);
    },

    MemberExpression: function(node) {
      if (!isDisplayNameDeclaration(node.property)) {
        return;
      }
      var component = componentList.getByName(node.object.name);
      if (!component) {
        return;
      }
      markDisplayNameAsDeclared(component.node);
    },

    MethodDefinition: function(node) {
      if (!isDisplayNameDeclaration(node.key)) {
        return;
      }
      markDisplayNameAsDeclared(node);
    },

    ClassDeclaration: function(node) {
      if (!acceptTranspilerName || !hasTranspilerName(node)) {
        return;
      }
      markDisplayNameAsDeclared(node);
    },

    ObjectExpression: function(node) {
      // Search for the displayName declaration
      node.properties.forEach(function(property) {
        if (!isDisplayNameDeclaration(property.key)) {
          return;
        }
        markDisplayNameAsDeclared(node);
      });
      // Has transpiler name
      if (acceptTranspilerName && hasTranspilerName(node)) {
        markDisplayNameAsDeclared(node);
      }

      if (componentUtil.isComponentDefinition(node)) {
        componentList.set(context, node, {
          isReactComponent: true
        });
      }
    },

    'Program:exit': function() {
      var list = componentList.getList();
      // Report missing display name for all classes
      for (var component in list) {
        if (!list.hasOwnProperty(component) || !mustBeValidated(list[component])) {
          continue;
        }
        reportMissingDisplayName(list[component]);
      }
    },

    ReturnStatement: function(node) {
      if (!componentUtil.isReactComponent(context, node)) {
        return;
      }
      componentList.set(context, node, {
        isReactComponent: true
      });
    }
  };

};

module.exports.schema = [{
  type: 'object',
  properties: {
    acceptTranspilerName: {
      type: 'boolean'
    }
  },
  additionalProperties: false
}];

},{"../util/component":24}],7:[function(require,module,exports){
/**
 * @fileoverview Enforce boolean attributes notation in JSX
 * @author Yannick Croissant
 */
'use strict';

// ------------------------------------------------------------------------------
// Rule Definition
// ------------------------------------------------------------------------------

module.exports = function(context) {

  var configuration = context.options[0] || {};

  var NEVER_MESSAGE = 'Value must be omitted for boolean attributes';
  var ALWAYS_MESSAGE = 'Value must be set for boolean attributes';

  return {
    JSXAttribute: function(node) {
      switch (configuration) {
        case 'always':
          if (node.value === null) {
            context.report(node, ALWAYS_MESSAGE);
          }
          break;
        case 'never':
          if (node.value && node.value.type === 'JSXExpressionContainer' && node.value.expression.value === true) {
            context.report(node, NEVER_MESSAGE);
          }
          break;
        default:
          break;
      }
    }
  };
};

module.exports.schema = [{
  enum: ['always', 'never']
}];

},{}],8:[function(require,module,exports){
/**
 * @fileoverview Disallow undeclared variables in JSX
 * @author Yannick Croissant
 */

'use strict';

/**
 * Checks if a node name match the JSX tag convention.
 * @param {String} name - Name of the node to check.
 * @returns {boolean} Whether or not the node name match the JSX tag convention.
 */
var tagConvention = /^[a-z]|\-/;
function isTagName(name) {
  return tagConvention.test(name);
}

// ------------------------------------------------------------------------------
// Rule Definition
// ------------------------------------------------------------------------------

module.exports = function(context) {

  /**
   * Compare an identifier with the variables declared in the scope
   * @param {ASTNode} node - Identifier or JSXIdentifier node
   * @returns {void}
   */
  function checkIdentifierInJSX(node) {
    var scope = context.getScope();
    var variables = scope.variables;
    var i;
    var len;

    while (scope.type !== 'global') {
      scope = scope.upper;
      variables = scope.variables.concat(variables);
    }
    if (scope.childScopes.length) {
      variables = scope.childScopes[0].variables.concat(variables);
      // Temporary fix for babel-eslint
      if (scope.childScopes[0].childScopes.length) {
        variables = scope.childScopes[0].childScopes[0].variables.concat(variables);
      }
    }

    for (i = 0, len = variables.length; i < len; i++) {
      if (variables[i].name === node.name) {
        return;
      }
    }

    context.report(node, '\'' + node.name + '\' is not defined.');
  }

  return {
        JSXOpeningElement: function(node) {
          if (isTagName(node.name.name)) {
            return;
          }
          checkIdentifierInJSX(node.name);
        }
  };

};

module.exports.schema = [];

},{}],9:[function(require,module,exports){
/**
 * @fileoverview Enforce props quotes style
 * @author Matt DuVall <http://www.mattduvall.com/>, Brandon Payton, Yannick Croissant
 */
'use strict';

// ------------------------------------------------------------------------------
// Constants
// ------------------------------------------------------------------------------

var QUOTE_SETTINGS = {
  double: {
    quote: '"',
    alternateQuote: '\'',
    description: 'doublequote'
  },
  single: {
    quote: '\'',
    alternateQuote: '"',
    description: 'singlequote'
  }
};

var AVOID_ESCAPE = 'avoid-escape';

// ------------------------------------------------------------------------------
// Rule Definition
// ------------------------------------------------------------------------------

module.exports = function(context) {

  /**
   * Validate that a string passed in is surrounded by the specified character
   * @param  {string} val The text to check.
   * @param  {string} character The character to see if it's surrounded by.
   * @returns {boolean} True if the text is surrounded by the character, false if not.
   * @private
   */
  function isSurroundedBy(val, character) {
    return val[0] === character && val[val.length - 1] === character;
  }

  return {

    Literal: function(node) {
      if (node.parent.type !== 'JSXAttribute') {
        return;
      }
      var val = node.value;
      var rawVal = node.raw;
      var quoteOption = context.options[0];
      var settings = QUOTE_SETTINGS[quoteOption];
      var avoidEscape = context.options[1] === AVOID_ESCAPE;
      var isValid;

      if (settings && typeof val === 'string') {
        isValid = isSurroundedBy(rawVal, settings.quote);

        if (!isValid && avoidEscape) {
          isValid = isSurroundedBy(rawVal, settings.alternateQuote) && rawVal.indexOf(settings.quote) >= 0;
        }

        if (!isValid) {
          context.report(node, 'JSX attributes must use ' + settings.description + '.');
        }
      }
    }
  };

};

module.exports.schema = [{
  enum: ['single', 'double']
}, {
  enum: ['avoid-escape']
}];

},{}],10:[function(require,module,exports){
/**
 * @fileoverview Enforce propTypes declarations alphabetical sorting
 */
'use strict';

// ------------------------------------------------------------------------------
// Rule Definition
// ------------------------------------------------------------------------------

module.exports = function(context) {

  var configuration = context.options[0] || {};
  var ignoreCase = configuration.ignoreCase || false;

  /**
   * Checks if node is `propTypes` declaration
   * @param {ASTNode} node The AST node being checked.
   * @returns {Boolean} True if node is `propTypes` declaration, false if not.
   */
  function isPropTypesDeclaration(node) {

    // Special case for class properties
    // (babel-eslint does not expose property name so we have to rely on tokens)
    if (node.type === 'ClassProperty') {
      var tokens = context.getFirstTokens(node, 2);
      if (tokens[0].value === 'propTypes' || tokens[1].value === 'propTypes') {
        return true;
      }
      return false;
    }

    return Boolean(
      node &&
      node.name === 'propTypes'
    );
  }

  function getKey(node) {
    return node.key.type === 'Identifier' ? node.key.name : node.key.value;
  }

  /**
   * Checks if propTypes declarations are sorted
   * @param {Array} declarations The array of AST nodes being checked.
   * @returns {void}
   */
  function checkSorted(declarations) {
    declarations.reduce(function(prev, curr) {
      var prevPropName = getKey(prev);
      var currenPropName = getKey(curr);

      if (ignoreCase) {
        prevPropName = prevPropName.toLowerCase();
        currenPropName = currenPropName.toLowerCase();
      }

      if (currenPropName < prevPropName) {
        context.report(curr, 'Prop types declarations should be sorted alphabetically');
        return prev;
      }

      return curr;
    }, declarations[0]);
  }

  return {
    ClassProperty: function(node) {
      if (isPropTypesDeclaration(node) && node.value && node.value.type === 'ObjectExpression') {
        checkSorted(node.value.properties);
      }
    },

    MemberExpression: function(node) {
      if (isPropTypesDeclaration(node.property)) {
        var right = node.parent.right;
        if (right && right.type === 'ObjectExpression') {
          checkSorted(right.properties);
        }
      }
    },

    ObjectExpression: function(node) {
      node.properties.forEach(function(property) {
        if (!isPropTypesDeclaration(property.key)) {
          return;
        }
        if (property.value.type === 'ObjectExpression') {
          checkSorted(property.value.properties);
        }
      });
    }

  };
};

module.exports.schema = [{
  type: 'object',
  properties: {
    ignoreCase: {
      type: 'boolean'
    }
  },
  additionalProperties: false
}];

},{}],11:[function(require,module,exports){
/**
 * @fileoverview Enforce props alphabetical sorting
 * @author Ilya Volodin, Yannick Croissant
 */
'use strict';

// ------------------------------------------------------------------------------
// Rule Definition
// ------------------------------------------------------------------------------

module.exports = function(context) {

  var configuration = context.options[0] || {};
  var ignoreCase = configuration.ignoreCase || false;

  return {
    JSXOpeningElement: function(node) {
      node.attributes.reduce(function(memo, decl, idx, attrs) {
        if (decl.type === 'JSXSpreadAttribute') {
          return attrs[idx + 1];
        }

        var lastPropName = memo.name.name;
        var currenPropName = decl.name.name;

        if (ignoreCase) {
          lastPropName = lastPropName.toLowerCase();
          currenPropName = currenPropName.toLowerCase();
        }

        if (currenPropName < lastPropName) {
          context.report(decl, 'Props should be sorted alphabetically');
          return memo;
        }

        return decl;
      }, node.attributes[0]);
    }
  };
};

module.exports.schema = [{
  type: 'object',
  properties: {
    ignoreCase: {
      type: 'boolean'
    }
  },
  additionalProperties: false
}];

},{}],12:[function(require,module,exports){
/**
 * @fileoverview Prevent React to be marked as unused
 * @author Glen Mailer
 */
'use strict';

var variableUtil = require('../util/variable');

// ------------------------------------------------------------------------------
// Rule Definition
// ------------------------------------------------------------------------------

var JSX_ANNOTATION_REGEX = /^\*\s*@jsx\s+([^\s]+)/;

module.exports = function(context) {

  var config = context.options[0] || {};
  var id = config.pragma || 'React';

  // --------------------------------------------------------------------------
  // Public
  // --------------------------------------------------------------------------

  return {

    JSXOpeningElement: function() {
      variableUtil.markVariableAsUsed(context, id);
    },

    BlockComment: function(node) {
      var matches = JSX_ANNOTATION_REGEX.exec(node.value);
      if (!matches) {
        return;
      }
      id = matches[1].split('.')[0];
    }

  };

};

module.exports.schema = [{
  type: 'object',
  properties: {
    pragma: {
      type: 'string'
    }
  },
  additionalProperties: false
}];

},{"../util/variable":25}],13:[function(require,module,exports){
/**
 * @fileoverview Prevent variables used in JSX to be marked as unused
 * @author Yannick Croissant
 */
'use strict';

var variableUtil = require('../util/variable');

// ------------------------------------------------------------------------------
// Rule Definition
// ------------------------------------------------------------------------------

module.exports = function(context) {

  return {
    JSXExpressionContainer: function(node) {
      if (node.expression.type !== 'Identifier') {
        return;
      }
      variableUtil.markVariableAsUsed(context, node.expression.name);
    },

    JSXIdentifier: function(node) {
      if (node.parent.type === 'JSXAttribute') {
        return;
      }
      variableUtil.markVariableAsUsed(context, node.name);
    }

  };

};

module.exports.schema = [];

},{"../util/variable":25}],14:[function(require,module,exports){
/**
 * @fileoverview Prevent usage of setState in componentDidMount
 * @author Yannick Croissant
 */
'use strict';

// ------------------------------------------------------------------------------
// Rule Definition
// ------------------------------------------------------------------------------

module.exports = function(context) {

  var mode = context.options[0] || 'never';

  // --------------------------------------------------------------------------
  // Public
  // --------------------------------------------------------------------------

  return {

    CallExpression: function(node) {
      var callee = node.callee;
      if (
        callee.type !== 'MemberExpression' ||
        callee.object.type !== 'ThisExpression' ||
        callee.property.name !== 'setState'
      ) {
        return;
      }
      var ancestors = context.getAncestors(callee).reverse();
      var depth = 0;
      for (var i = 0, j = ancestors.length; i < j; i++) {
        if (/Function(Expression|Declaration)$/.test(ancestors[i].type)) {
          depth++;
        }
        if (
          ancestors[i].type !== 'Property' ||
          ancestors[i].key.name !== 'componentDidMount' ||
          (mode === 'allow-in-func' && depth > 1)
        ) {
          continue;
        }
        context.report(callee, 'Do not use setState in componentDidMount');
        break;
      }
    }
  };

};

module.exports.schema = [{
  enum: ['allow-in-func']
}];

},{}],15:[function(require,module,exports){
/**
 * @fileoverview Prevent usage of setState in componentDidUpdate
 * @author Yannick Croissant
 */
'use strict';

// ------------------------------------------------------------------------------
// Rule Definition
// ------------------------------------------------------------------------------

module.exports = function(context) {

  // --------------------------------------------------------------------------
  // Public
  // --------------------------------------------------------------------------

  return {

    CallExpression: function(node) {
      var callee = node.callee;
      if (callee.type !== 'MemberExpression') {
        return;
      }
      if (callee.object.type !== 'ThisExpression' || callee.property.name !== 'setState') {
        return;
      }
      var ancestors = context.getAncestors(callee);
      for (var i = 0, j = ancestors.length; i < j; i++) {
        if (ancestors[i].type !== 'Property' || ancestors[i].key.name !== 'componentDidUpdate') {
          continue;
        }
        context.report(callee, 'Do not use setState in componentDidUpdate');
      }
    }
  };

};

module.exports.schema = [];

},{}],16:[function(require,module,exports){
/**
 * @fileoverview Prevent multiple component definition per file
 * @author Yannick Croissant
 */
'use strict';

var componentUtil = require('../util/component');
var ComponentList = componentUtil.List;

// ------------------------------------------------------------------------------
// Rule Definition
// ------------------------------------------------------------------------------

module.exports = function(context) {

  var componentList = new ComponentList();

  var MULTI_COMP_MESSAGE = 'Declare only one React component per file';

  // --------------------------------------------------------------------------
  // Public
  // --------------------------------------------------------------------------

  return {
    'Program:exit': function() {
      if (componentList.count() <= 1) {
        return;
      }

      var list = componentList.getList();
      var i = 0;

      for (var component in list) {
        if (!list.hasOwnProperty(component) || ++i === 1) {
          continue;
        }
        context.report(list[component].node, MULTI_COMP_MESSAGE);
      }
    },

    ReturnStatement: function(node) {
      if (!componentUtil.isReactComponent(context, node)) {
        return;
      }
      componentList.set(context, node);
    }
  };
};

module.exports.schema = [];

},{"../util/component":24}],17:[function(require,module,exports){
/**
 * @fileoverview Prevent usage of unknown DOM property
 * @author Yannick Croissant
 */
'use strict';

// ------------------------------------------------------------------------------
// Constants
// ------------------------------------------------------------------------------

var UNKNOWN_MESSAGE = 'Unknown property \'{{name}}\' found, use \'{{standardName}}\' instead';

var DOM_ATTRIBUTE_NAMES = {
  'accept-charset': 'acceptCharset',
  class: 'className',
  for: 'htmlFor',
  'http-equiv': 'httpEquiv'
};

var DOM_PROPERTY_NAMES = [
  'acceptCharset', 'accessKey', 'allowFullScreen', 'allowTransparency', 'autoComplete', 'autoFocus', 'autoPlay',
  'cellPadding', 'cellSpacing', 'charSet', 'classID', 'className', 'colSpan', 'contentEditable', 'contextMenu',
  'crossOrigin', 'dateTime', 'encType', 'formAction', 'formEncType', 'formMethod', 'formNoValidate', 'formTarget',
  'frameBorder', 'hrefLang', 'htmlFor', 'httpEquiv', 'marginHeight', 'marginWidth', 'maxLength', 'mediaGroup',
  'noValidate', 'radioGroup', 'readOnly', 'rowSpan', 'spellCheck', 'srcDoc', 'srcSet', 'tabIndex', 'useMap',
  'itemProp', 'itemScope', 'itemType', 'itemRef', 'itemID'
];

// ------------------------------------------------------------------------------
// Helpers
// ------------------------------------------------------------------------------

/**
 * Checks if a node name match the JSX tag convention.
 * @param {String} name - Name of the node to check.
 * @returns {boolean} Whether or not the node name match the JSX tag convention.
 */
var tagConvention = /^[a-z]|\-/;
function isTagName(name) {
  return tagConvention.test(name);
}

/**
 * Get the standard name of the attribute.
 * @param {String} name - Name of the attribute.
 * @returns {String} The standard name of the attribute.
 */
function getStandardName(name) {
  if (DOM_ATTRIBUTE_NAMES[name]) {
    return DOM_ATTRIBUTE_NAMES[name];
  }
  var i;
  var found = DOM_PROPERTY_NAMES.some(function(element, index) {
    i = index;
    return element.toLowerCase() === name;
  });
  return found ? DOM_PROPERTY_NAMES[i] : null;
}

// ------------------------------------------------------------------------------
// Rule Definition
// ------------------------------------------------------------------------------

module.exports = function(context) {

  return {

    JSXAttribute: function(node) {
      var standardName = getStandardName(node.name.name);
      if (!isTagName(node.parent.name.name) || !standardName) {
        return;
      }
      context.report(node, UNKNOWN_MESSAGE, {
        name: node.name.name,
        standardName: standardName
      });
    }
  };

};

module.exports.schema = [];

},{}],18:[function(require,module,exports){
/**
 * @fileoverview Prevent missing props validation in a React component definition
 * @author Yannick Croissant
 */
'use strict';

// As for exceptions for props.children or props.className (and alike) look at
// https://github.com/yannickcr/eslint-plugin-react/issues/7

var componentUtil = require('../util/component');
var ComponentList = componentUtil.List;

// ------------------------------------------------------------------------------
// Rule Definition
// ------------------------------------------------------------------------------

module.exports = function(context) {

  var configuration = context.options[0] || {};
  var ignored = configuration.ignore || [];

  var componentList = new ComponentList();

  var MISSING_MESSAGE = '\'{{name}}\' is missing in props validation';
  var MISSING_MESSAGE_NAMED_COMP = '\'{{name}}\' is missing in props validation for {{component}}';

  /**
   * Checks if we are using a prop
   * @param {ASTNode} node The AST node being checked.
   * @returns {Boolean} True if we are using a prop, false if not.
   */
  function isPropTypesUsage(node) {
    return Boolean(
      node.object.type === 'ThisExpression' &&
      node.property.name === 'props'
    );
  }

  /**
   * Checks if we are declaring a prop
   * @param {ASTNode} node The AST node being checked.
   * @returns {Boolean} True if we are declaring a prop, false if not.
   */
  function isPropTypesDeclaration(node) {

    // Special case for class properties
    // (babel-eslint does not expose property name so we have to rely on tokens)
    if (node.type === 'ClassProperty') {
      var tokens = context.getFirstTokens(node, 2);
      if (
        tokens[0].value === 'propTypes' ||
        (tokens[1] && tokens[1].value === 'propTypes')
      ) {
        return true;
      }
      return false;
    }

    return Boolean(
      node &&
      node.name === 'propTypes'
    );

  }

  /**
   * Checks if the prop is ignored
   * @param {String} name Name of the prop to check.
   * @returns {Boolean} True if the prop is ignored, false if not.
   */
  function isIgnored(name) {
    return ignored.indexOf(name) !== -1;
  }

  /**
   * Checks if the component must be validated
   * @param {Object} component The component to process
   * @returns {Boolean} True if the component must be validated, false if not.
   */
  function mustBeValidated(component) {
    return (
      component &&
      component.isReactComponent &&
      component.usedPropTypes &&
      !component.ignorePropsValidation
    );
  }

  /**
   * Internal: Checks if the prop is declared
   * @param {Object} declaredPropTypes Description of propTypes declared in the current component
   * @param {String[]} keyList Dot separated name of the prop to check.
   * @returns {Boolean} True if the prop is declared, false if not.
   */
  function _isDeclaredInComponent(declaredPropTypes, keyList) {
    for (var i = 0, j = keyList.length; i < j; i++) {
      var key = keyList[i];
      var propType = (
        // Check if this key is declared
        declaredPropTypes[key] ||
        // If not, check if this type accepts any key
        declaredPropTypes.__ANY_KEY__
      );

      if (!propType) {
        // If it's a computed property, we can't make any further analysis, but is valid
        return key === '__COMPUTED_PROP__';
      }
      if (propType === true) {
        return true;
      }
      // Consider every children as declared
      if (propType.children === true) {
        return true;
      }
      if (propType.acceptedProperties) {
        return key in propType.acceptedProperties;
      }
      if (propType.type === 'union') {
        // If we fall in this case, we know there is at least one complex type in the union
        if (i + 1 >= j) {
          // this is the last key, accept everything
          return true;
        }
        // non trivial, check all of them
        var unionTypes = propType.children;
        var unionPropType = {};
        for (var k = 0, z = unionTypes.length; k < z; k++) {
          unionPropType[key] = unionTypes[k];
          var isValid = _isDeclaredInComponent(
            unionPropType,
            keyList.slice(i)
          );
          if (isValid) {
            return true;
          }
        }

        // every possible union were invalid
        return false;
      }
      declaredPropTypes = propType.children;
    }
    return true;
  }

  /**
   * Checks if the prop is declared
   * @param {Object} component The component to process
   * @param {String[]} names List of names of the prop to check.
   * @returns {Boolean} True if the prop is declared, false if not.
   */
  function isDeclaredInComponent(component, names) {
    return _isDeclaredInComponent(
      component.declaredPropTypes || {},
      names
    );
  }

  /**
   * Checks if the prop has spread operator.
   * @param {ASTNode} node The AST node being marked.
   * @returns {Boolean} True if the prop has spread operator, false if not.
   */
  function hasSpreadOperator(node) {
    var tokens = context.getTokens(node);
    return tokens.length && tokens[0].value === '...';
  }

  /**
   * Retrieve the name of a key node
   * @param {ASTNode} node The AST node with the key.
   * @return {string} the name of the key
   */
  function getKeyValue(node) {
    var key = node.key;
    return key.type === 'Identifier' ? key.name : key.value;
  }

  /**
   * Iterates through a properties node, like a customized forEach.
   * @param {Object[]} properties Array of properties to iterate.
   * @param {Function} fn Function to call on each property, receives property key
      and property value. (key, value) => void
   */
  function iterateProperties(properties, fn) {
    if (properties.length && typeof fn === 'function') {
      for (var i = 0, j = properties.length; i < j; i++) {
        var node = properties[i];
        var key = getKeyValue(node);

        var value = node.value;
        fn(key, value);
      }
    }
  }

  /**
   * Creates the representation of the React propTypes for the component.
   * The representation is used to verify nested used properties.
   * @param {ASTNode} value Node of the React.PropTypes for the desired propery
   * @return {Object|Boolean} The representation of the declaration, true means
   *    the property is declared without the need for further analysis.
   */
  function buildReactDeclarationTypes(value) {
    if (
      value.type === 'MemberExpression' &&
      value.property &&
      value.property.name &&
      value.property.name === 'isRequired'
    ) {
      value = value.object;
    }

    // Verify React.PropTypes that are functions
    if (
      value.type === 'CallExpression' &&
      value.callee &&
      value.callee.property &&
      value.callee.property.name &&
      value.arguments &&
      value.arguments.length > 0
    ) {
      var callName = value.callee.property.name;
      var argument = value.arguments[0];
      switch (callName) {
        case 'shape':
          if (argument.type !== 'ObjectExpression') {
            // Invalid proptype or cannot analyse statically
            return true;
          }
          var shapeTypeDefinition = {
            type: 'shape',
            children: {}
          };
          iterateProperties(argument.properties, function(childKey, childValue) {
            shapeTypeDefinition.children[childKey] = buildReactDeclarationTypes(childValue);
          });
          return shapeTypeDefinition;
        case 'arrayOf':
          return {
            type: 'array',
            children: {
              // Accept only array prototype and computed properties
              __ANY_KEY__: {
                acceptedProperties: Array.prototype
              },
              __COMPUTED_PROP__: buildReactDeclarationTypes(argument)
            }
          };
        case 'objectOf':
          return {
            type: 'object',
            children: {
              __ANY_KEY__: buildReactDeclarationTypes(argument)
            }
          };
        case 'oneOfType':
          if (
            !argument.elements ||
            !argument.elements.length
          ) {
            // Invalid proptype or cannot analyse statically
            return true;
          }
          var unionTypeDefinition = {
            type: 'union',
            children: []
          };
          for (var i = 0, j = argument.elements.length; i < j; i++) {
            var type = buildReactDeclarationTypes(argument.elements[i]);
            // keep only complex type
            if (type !== true) {
              if (type.children === true) {
                // every child is accepted for one type, abort type analysis
                unionTypeDefinition.children = true;
                return unionTypeDefinition;
              }
              unionTypeDefinition.children.push(type);
            }
          }
          if (unionTypeDefinition.length === 0) {
            // no complex type found, simply accept everything
            return true;
          }
          return unionTypeDefinition;
        case 'instanceOf':
          return {
            type: 'instance',
            // Accept all children because we can't know what type they are
            children: true
          };
        case 'oneOf':
        default:
          return true;
      }
    }
    if (
      value.type === 'MemberExpression' &&
      value.property &&
      value.property.name
    ) {
      var name = value.property.name;
      // React propTypes with limited possible properties
      var propertiesMap = {
        array: Array.prototype,
        bool: Boolean.prototype,
        func: Function.prototype,
        number: Number.prototype,
        string: String.prototype
      };
      if (name in propertiesMap) {
        return {
          type: name,
          children: {
            __ANY_KEY__: {
              acceptedProperties: propertiesMap[name]
            }
          }
        };
      }
    }
    // Unknown property or accepts everything (any, object, ...)
    return true;
  }

  /**
   * Retrieve the name of a property node
   * @param {ASTNode} node The AST node with the property.
   * @return {string} the name of the property or undefined if not found
   */
  function getPropertyName(node) {
    var property = node.property;
    if (property) {
      switch (property.type) {
        case 'Identifier':
          if (node.computed) {
            return '__COMPUTED_PROP__';
          }
          return property.name;
        case 'MemberExpression':
          return void 0;
        case 'Literal':
          // Accept computed properties that are literal strings
          if (typeof property.value === 'string') {
            return property.value;
          }
          // falls through
        default:
          if (node.computed) {
            return '__COMPUTED_PROP__';
          }
          break;
      }
    }
  }

  /**
   * Mark a prop type as used
   * @param {ASTNode} node The AST node being marked.
   */
  function markPropTypesAsUsed(node, parentNames) {
    parentNames = parentNames || [];
    var type;
    var name;
    var allNames;
    var properties;
    switch (node.type) {
      case 'MemberExpression':
        name = getPropertyName(node.parent);
        if (name) {
          allNames = parentNames.concat(name);
          if (node.parent.type === 'MemberExpression') {
            markPropTypesAsUsed(node.parent, allNames);
          }
          // Do not mark computed props as used.
          type = name !== '__COMPUTED_PROP__' ? 'direct' : null;
        } else if (
          node.parent.parent.declarations &&
          node.parent.parent.declarations[0].id.properties &&
          getKeyValue(node.parent.parent.declarations[0].id.properties[0])
        ) {
          type = 'destructuring';
          properties = node.parent.parent.declarations[0].id.properties;
        }
        break;
      case 'VariableDeclarator':
        for (var i = 0, j = node.id.properties.length; i < j; i++) {
          if (
            (node.id.properties[i].key.name !== 'props' && node.id.properties[i].key.value !== 'props') ||
            node.id.properties[i].value.type !== 'ObjectPattern'
          ) {
            continue;
          }
          type = 'destructuring';
          properties = node.id.properties[i].value.properties;
          break;
        }
        break;
      default:
        throw new Error(node.type + ' ASTNodes are not handled by markPropTypesAsUsed');
    }

    var component = componentList.getByNode(context, node);
    var usedPropTypes = component && component.usedPropTypes || [];

    switch (type) {
      case 'direct':
        // Ignore Object methods
        if (Object.prototype[name]) {
          break;
        }
        usedPropTypes.push({
          name: name,
          allNames: allNames,
          node: node.parent.property
        });
        break;
      case 'destructuring':
        for (var k = 0, l = properties.length; k < l; k++) {
          if (hasSpreadOperator(properties[k])) {
            continue;
          }
          var propName = getKeyValue(properties[k]);

          var currentNode = node;
          allNames = [];
          while (currentNode.property && currentNode.property.name !== 'props') {
            allNames.unshift(currentNode.property.name);
            currentNode = currentNode.object;
          }
          allNames.push(propName);

          if (propName) {
            usedPropTypes.push({
              name: propName,
              allNames: allNames,
              node: properties[k]
            });
          }
        }
        break;
      default:
        break;
    }

    componentList.set(context, node, {
      usedPropTypes: usedPropTypes
    });
  }

  /**
   * Mark a prop type as declared
   * @param {ASTNode} node The AST node being checked.
   * @param {propTypes} node The AST node containing the proptypes
   */
  function markPropTypesAsDeclared(node, propTypes) {
    var component = componentList.getByNode(context, node);
    var declaredPropTypes = component && component.declaredPropTypes || {};
    var ignorePropsValidation = false;

    switch (propTypes && propTypes.type) {
      case 'ObjectExpression':
        iterateProperties(propTypes.properties, function(key, value) {
          declaredPropTypes[key] = buildReactDeclarationTypes(value);
        });
        break;
      case 'MemberExpression':
        var curDeclaredPropTypes = declaredPropTypes;
        // Walk the list of properties, until we reach the assignment
        // ie: ClassX.propTypes.a.b.c = ...
        while (
          propTypes &&
          propTypes.parent.type !== 'AssignmentExpression' &&
          propTypes.property &&
          curDeclaredPropTypes
        ) {
          var propName = propTypes.property.name;
          if (propName in curDeclaredPropTypes) {
            curDeclaredPropTypes = curDeclaredPropTypes[propName].children;
            propTypes = propTypes.parent;
          } else {
            // This will crash at runtime because we haven't seen this key before
            // stop this and do not declare it
            propTypes = null;
          }
        }
        if (propTypes) {
          curDeclaredPropTypes[propTypes.property.name] =
            buildReactDeclarationTypes(propTypes.parent.right);
        }
        break;
      case null:
        break;
      default:
        ignorePropsValidation = true;
        break;
    }

    componentList.set(context, node, {
      declaredPropTypes: declaredPropTypes,
      ignorePropsValidation: ignorePropsValidation
    });
  }

  /**
   * Reports undeclared proptypes for a given component
   * @param {Object} component The component to process
   */
  function reportUndeclaredPropTypes(component) {
    var allNames;
    for (var i = 0, j = component.usedPropTypes.length; i < j; i++) {
      allNames = component.usedPropTypes[i].allNames;
      if (
        isIgnored(allNames[0]) ||
        isDeclaredInComponent(component, allNames)
      ) {
        continue;
      }
      context.report(
        component.usedPropTypes[i].node,
        component.name === componentUtil.DEFAULT_COMPONENT_NAME ? MISSING_MESSAGE : MISSING_MESSAGE_NAMED_COMP, {
          name: allNames.join('.').replace(/\.__COMPUTED_PROP__/g, '[]'),
          component: component.name
        }
      );
    }
  }

  // --------------------------------------------------------------------------
  // Public
  // --------------------------------------------------------------------------

  return {

    ClassProperty: function(node) {
      if (!isPropTypesDeclaration(node)) {
        return;
      }

      markPropTypesAsDeclared(node, node.value);
    },

    VariableDeclarator: function(node) {
      if (!node.init || node.init.type !== 'ThisExpression' || node.id.type !== 'ObjectPattern') {
        return;
      }
      markPropTypesAsUsed(node);
    },

    MemberExpression: function(node) {
      var type;
      if (isPropTypesUsage(node)) {
        type = 'usage';
      } else if (isPropTypesDeclaration(node.property)) {
        type = 'declaration';
      }

      switch (type) {
        case 'usage':
          markPropTypesAsUsed(node);
          break;
        case 'declaration':
          var component = componentList.getByName(node.object.name);
          if (!component) {
            return;
          }
          markPropTypesAsDeclared(component.node, node.parent.right || node.parent);
          break;
        default:
          break;
      }
    },

    MethodDefinition: function(node) {
      if (!isPropTypesDeclaration(node.key)) {
        return;
      }

      var i = node.value.body.body.length - 1;
      for (; i >= 0; i--) {
        if (node.value.body.body[i].type === 'ReturnStatement') {
          break;
        }
      }

      markPropTypesAsDeclared(node, node.value.body.body[i].argument);
    },

    ObjectExpression: function(node) {
      // Search for the proptypes declaration
      node.properties.forEach(function(property) {
        if (!isPropTypesDeclaration(property.key)) {
          return;
        }
        markPropTypesAsDeclared(node, property.value);
      });

      if (componentUtil.isComponentDefinition(node)) {
        componentList.set(context, node, {
          isReactComponent: true
        });
      }
    },

    'Program:exit': function() {
      var list = componentList.getList();
      // Report undeclared proptypes for all classes
      for (var component in list) {
        if (!list.hasOwnProperty(component) || !mustBeValidated(list[component])) {
          continue;
        }
        reportUndeclaredPropTypes(list[component]);
      }
    },

    ReturnStatement: function(node) {
      if (!componentUtil.isReactComponent(context, node)) {
        return;
      }
      componentList.set(context, node, {
        isReactComponent: true
      });
    }
  };

};

module.exports.schema = [{
  type: 'object',
  properties: {
    ignore: {
      type: 'array',
      items: {
        type: 'string'
      }
    }
  },
  additionalProperties: false
}];

},{"../util/component":24}],19:[function(require,module,exports){
/**
 * @fileoverview Prevent missing React when using JSX
 * @author Glen Mailer
 */
'use strict';

var variableUtil = require('../util/variable');

// -----------------------------------------------------------------------------
// Rule Definition
// -----------------------------------------------------------------------------

var JSX_ANNOTATION_REGEX = /^\*\s*@jsx\s+([^\s]+)/;

module.exports = function(context) {

  var id = 'React';
  var NOT_DEFINED_MESSAGE = '\'{{name}}\' must be in scope when using JSX';

  return {

    JSXOpeningElement: function(node) {
      var variables = variableUtil.variablesInScope(context);
      if (variableUtil.findVariable(variables, id)) {
        return;
      }
      context.report(node, NOT_DEFINED_MESSAGE, {
        name: id
      });
    },

    BlockComment: function(node) {
      var matches = JSX_ANNOTATION_REGEX.exec(node.value);
      if (!matches) {
        return;
      }
      id = matches[1].split('.')[0];
    }

  };

};

module.exports.schema = [];

},{"../util/variable":25}],20:[function(require,module,exports){
/**
 * @fileoverview Restrict file extensions that may be required
 * @author Scott Andrews
 */
'use strict';

var path = require('path');

// ------------------------------------------------------------------------------
// Constants
// ------------------------------------------------------------------------------

var DEFAULTS = {
  extentions: ['.jsx']
};

// ------------------------------------------------------------------------------
// Rule Definition
// ------------------------------------------------------------------------------

module.exports = function(context) {

  function isRequire(expression) {
    return expression.callee.name === 'require';
  }

  function getId(expression) {
    return expression.arguments[0] && expression.arguments[0].value;
  }

  function getExtension(id) {
    return path.extname(id || '');
  }

  function getExtentionsConfig() {
    return context.options[0] && context.options[0].extensions || DEFAULTS.extentions;
  }

  var forbiddenExtensions = getExtentionsConfig().reduce(function (extensions, extension) {
    extensions[extension] = true;
    return extensions;
  }, Object.create(null));

  function isForbiddenExtension(ext) {
    return ext in forbiddenExtensions;
  }

  // --------------------------------------------------------------------------
  // Public
  // --------------------------------------------------------------------------

  return {

    CallExpression: function(node) {
      if (isRequire(node)) {
        var ext = getExtension(getId(node));
        if (isForbiddenExtension(ext)) {
          context.report(node, 'Unable to require module with extension \'' + ext + '\'');
        }
      }
    }

  };

};

module.exports.schema = [{
  type: 'object',
  properties: {
    extensions: {
      type: 'array',
      items: {
        type: 'string'
      }
    }
  },
  additionalProperties: false
}];

},{"path":2}],21:[function(require,module,exports){
/**
 * @fileoverview Prevent extra closing tags for components without children
 * @author Yannick Croissant
 */
'use strict';

// ------------------------------------------------------------------------------
// Rule Definition
// ------------------------------------------------------------------------------

module.exports = function(context) {

  var tagConvention = /^[a-z]|\-/;
  function isTagName(name) {
    return tagConvention.test(name);
  }

  function isComponent(node) {
    return node.name && node.name.type === 'JSXIdentifier' && !isTagName(node.name.name);
  }

  function hasChildren(node) {
    var childrens = node.parent.children;
    if (
      !childrens.length ||
      (childrens.length === 1 && childrens[0].type === 'Literal' && !childrens[0].value.trim())
    ) {
      return false;
    }
    return true;
  }

  // --------------------------------------------------------------------------
  // Public
  // --------------------------------------------------------------------------

  return {

    JSXOpeningElement: function(node) {
      if (!isComponent(node) || node.selfClosing || hasChildren(node)) {
        return;
      }
      context.report(node, 'Empty components are self-closing');
    }
  };

};

module.exports.schema = [];

},{}],22:[function(require,module,exports){
/**
 * @fileoverview Enforce component methods order
 * @author Yannick Croissant
 */
'use strict';

var util = require('util');

var componentUtil = require('../util/component');
var ComponentList = componentUtil.List;

/**
 * Get the methods order from the default config and the user config
 * @param {Object} defaultConfig The default configuration.
 * @param {Object} userConfig The user configuration.
 * @returns {Array} Methods order
 */
function getMethodsOrder(defaultConfig, userConfig) {
  userConfig = userConfig || {};

  var groups = util._extend(defaultConfig.groups, userConfig.groups);
  var order = userConfig.order || defaultConfig.order;

  var config = [];
  var entry;
  for (var i = 0, j = order.length; i < j; i++) {
    entry = order[i];
    if (groups.hasOwnProperty(entry)) {
      config = config.concat(groups[entry]);
    } else {
      config.push(entry);
    }
  }

  return config;
}

// ------------------------------------------------------------------------------
// Rule Definition
// ------------------------------------------------------------------------------

module.exports = function(context) {

  var componentList = new ComponentList();
  var errors = {};

  var MISPOSITION_MESSAGE = '{{propA}} must be placed {{position}} {{propB}}';

  var methodsOrder = getMethodsOrder({
    order: [
      'lifecycle',
      'everything-else',
      'render'
    ],
    groups: {
      lifecycle: [
        'displayName',
        'propTypes',
        'contextTypes',
        'childContextTypes',
        'mixins',
        'statics',
        'defaultProps',
        'constructor',
        'getDefaultProps',
        'getInitialState',
        'getChildContext',
        'componentWillMount',
        'componentDidMount',
        'componentWillReceiveProps',
        'shouldComponentUpdate',
        'componentWillUpdate',
        'componentDidUpdate',
        'componentWillUnmount'
      ]
    }
  }, context.options[0]);

  /**
   * Checks if the component must be validated
   * @param {Object} component The component to process
   * @returns {Boolean} True if the component must be validated, false if not.
   */
  function mustBeValidated(component) {
    return (
      component &&
      component.isReactComponent &&
      !component.hasDisplayName
    );
  }

  // --------------------------------------------------------------------------
  // Public
  // --------------------------------------------------------------------------

  var regExpRegExp = /\/(.*)\/([g|y|i|m]*)/;

  /**
   * Get indexes of the matching patterns in methods order configuration
   * @param {String} method - Method name.
   * @returns {Array} The matching patterns indexes. Return [Infinity] if there is no match.
   */
  function getRefPropIndexes(method) {
    var isRegExp;
    var matching;
    var i;
    var j;
    var indexes = [];
    for (i = 0, j = methodsOrder.length; i < j; i++) {
      isRegExp = methodsOrder[i].match(regExpRegExp);
      if (isRegExp) {
        matching = new RegExp(isRegExp[1], isRegExp[2]).test(method);
      } else {
        matching = methodsOrder[i] === method;
      }
      if (matching) {
        indexes.push(i);
      }
    }

    // No matching pattern, return 'everything-else' index
    if (indexes.length === 0) {
      for (i = 0, j = methodsOrder.length; i < j; i++) {
        if (methodsOrder[i] === 'everything-else') {
          indexes.push(i);
        }
      }
    }

    // No matching pattern and no 'everything-else' group
    if (indexes.length === 0) {
      indexes.push(Infinity);
    }

    return indexes;
  }

  /**
   * Get properties name
   * @param {Object} node - Property.
   * @returns {String} Property name.
   */
  function getPropertyName(node) {

    // Special case for class properties
    // (babel-eslint does not expose property name so we have to rely on tokens)
    if (node.type === 'ClassProperty') {
      var tokens = context.getFirstTokens(node, 2);
      return tokens[1].type === 'Identifier' ? tokens[1].value : tokens[0].value;
    }

    return node.key.name;
  }

  /**
   * Store a new error in the error list
   * @param {Object} propA - Mispositioned property.
   * @param {Object} propB - Reference property.
   */
  function storeError(propA, propB) {
    // Initialize the error object if needed
    if (!errors[propA.index]) {
      errors[propA.index] = {
        node: propA.node,
        score: 0,
        closest: {
          distance: Infinity,
          ref: {
            node: null,
            index: 0
          }
        }
      };
    }
    // Increment the prop score
    errors[propA.index].score++;
    // Stop here if we already have a closer reference
    if (Math.abs(propA.index - propB.index) > errors[propA.index].closest.distance) {
      return;
    }
    // Update the closest reference
    errors[propA.index].closest.distance = Math.abs(propA.index - propB.index);
    errors[propA.index].closest.ref.node = propB.node;
    errors[propA.index].closest.ref.index = propB.index;
  }

  /**
   * Dedupe errors, only keep the ones with the highest score and delete the others
   */
  function dedupeErrors() {
    for (var i in errors) {
      if (!errors.hasOwnProperty(i)) {
        continue;
      }
      var index = errors[i].closest.ref.index;
      if (!errors[index]) {
        continue;
      }
      if (errors[i].score > errors[index].score) {
        delete errors[index];
      } else {
        delete errors[i];
      }
    }
  }

  /**
   * Report errors
   */
  function reportErrors() {
    dedupeErrors();

    var nodeA;
    var nodeB;
    var indexA;
    var indexB;
    for (var i in errors) {
      if (!errors.hasOwnProperty(i)) {
        continue;
      }

      nodeA = errors[i].node;
      nodeB = errors[i].closest.ref.node;
      indexA = i;
      indexB = errors[i].closest.ref.index;

      context.report(nodeA, MISPOSITION_MESSAGE, {
        propA: getPropertyName(nodeA),
        propB: getPropertyName(nodeB),
        position: indexA < indexB ? 'before' : 'after'
      });
    }
  }

  /**
   * Get properties for a given AST node
   * @param {ASTNode} node The AST node being checked.
   * @returns {Array} Properties array.
   */
  function getComponentProperties(node) {
    if (node.type === 'ClassDeclaration') {
      return node.body.body;
    }
    return node.properties;
  }

  /**
   * Compare two properties and find out if they are in the right order
   * @param {Array} propertiesNames Array containing all the properties names.
   * @param {String} propA First property name.
   * @param {String} propB Second property name.
   * @returns {Object} Object containing a correct true/false flag and the correct indexes for the two properties.
   */
  function comparePropsOrder(propertiesNames, propA, propB) {
    var i;
    var j;
    var k;
    var l;
    var refIndexA;
    var refIndexB;

    // Get references indexes (the correct position) for given properties
    var refIndexesA = getRefPropIndexes(propA);
    var refIndexesB = getRefPropIndexes(propB);

    // Get current indexes for given properties
    var classIndexA = propertiesNames.indexOf(propA);
    var classIndexB = propertiesNames.indexOf(propB);

    // Loop around the references indexes for the 1st property
    for (i = 0, j = refIndexesA.length; i < j; i++) {
      refIndexA = refIndexesA[i];

      // Loop around the properties for the 2nd property (for comparison)
      for (k = 0, l = refIndexesB.length; k < l; k++) {
        refIndexB = refIndexesB[k];

        if (
          // Comparing the same properties
          refIndexA === refIndexB ||
          // 1st property is placed before the 2nd one in reference and in current component
          refIndexA < refIndexB && classIndexA < classIndexB ||
          // 1st property is placed after the 2nd one in reference and in current component
          refIndexA > refIndexB && classIndexA > classIndexB
        ) {
          return {
            correct: true,
            indexA: classIndexA,
            indexB: classIndexB
          };
        }

      }
    }

    // We did not find any correct match between reference and current component
    return {
      correct: false,
      indexA: refIndexA,
      indexB: refIndexB
    };
  }

  /**
   * Check properties order from a properties list and store the eventual errors
   * @param {Array} properties Array containing all the properties.
   */
  function checkPropsOrder(properties) {
    var propertiesNames = properties.map(getPropertyName);
    var i;
    var j;
    var k;
    var l;
    var propA;
    var propB;
    var order;

    // Loop around the properties
    for (i = 0, j = propertiesNames.length; i < j; i++) {
      propA = propertiesNames[i];

      // Loop around the properties a second time (for comparison)
      for (k = 0, l = propertiesNames.length; k < l; k++) {
        propB = propertiesNames[k];

        // Compare the properties order
        order = comparePropsOrder(propertiesNames, propA, propB);

        // Continue to next comparison is order is correct
        if (order.correct === true) {
          continue;
        }

        // Store an error if the order is incorrect
        storeError({
          node: properties[i],
          index: order.indexA
        }, {
          node: properties[k],
          index: order.indexB
        });
      }
    }

  }

  return {
    'Program:exit': function() {
      var list = componentList.getList();

      for (var component in list) {
        if (!list.hasOwnProperty(component) || !mustBeValidated(list[component])) {
          continue;
        }
        var properties = getComponentProperties(list[component].node);
        checkPropsOrder(properties);
      }

      reportErrors();
    },

    ReturnStatement: function(node) {
      if (!componentUtil.isReactComponent(context, node)) {
        return;
      }
      componentList.set(context, node, {
        isReactComponent: true
      });
    }
  };

};

module.exports.schema = [{
  type: 'object',
  properties: {
    order: {
      type: 'array',
      items: {
        type: 'string'
      }
    },
    groups: {
      type: 'object',
      patternProperties: {
        '^.*$': {
          type: 'array',
          items: {
            type: 'string'
          }
        }
      }
    }
  },
  additionalProperties: false
}];

},{"../util/component":24,"util":5}],23:[function(require,module,exports){
/**
 * @fileoverview Prevent missing parentheses around multilines JSX
 * @author Yannick Croissant
 */
'use strict';

// ------------------------------------------------------------------------------
// Constants
// ------------------------------------------------------------------------------

var DEFAULTS = {
  declaration: true,
  assignment: true,
  return: true
};

// ------------------------------------------------------------------------------
// Rule Definition
// ------------------------------------------------------------------------------

module.exports = function(context) {

  function isParenthesised(node) {
    var previousToken = context.getTokenBefore(node);
    var nextToken = context.getTokenAfter(node);

    return previousToken && nextToken &&
      previousToken.value === '(' && previousToken.range[1] <= node.range[0] &&
      nextToken.value === ')' && nextToken.range[0] >= node.range[1];
  }

  function isMultilines(node) {
    return node.loc.start.line !== node.loc.end.line;
  }

  function check(node) {
    if (!node || node.type !== 'JSXElement') {
      return;
    }

    if (!isParenthesised(node) && isMultilines(node)) {
      context.report(node, 'Missing parentheses around multilines JSX');
    }
  }

  function isEnabled(type) {
    var userOptions = context.options[0] || {};
    if (({}).hasOwnProperty.call(userOptions, type)) {
      return userOptions[type];
    }
    return DEFAULTS[type];
  }

  // --------------------------------------------------------------------------
  // Public
  // --------------------------------------------------------------------------

  return {

    VariableDeclarator: function(node) {
      if (isEnabled('declaration')) {
        check(node.init);
      }
    },

    AssignmentExpression: function(node) {
      if (isEnabled('assignment')) {
        check(node.right);
      }
    },

    ReturnStatement: function(node) {
      if (isEnabled('return')) {
        check(node.argument);
      }
    }
  };

};

module.exports.schema = [{
  type: 'object',
  properties: {
    declaration: {
      type: 'boolean'
    },
    assignment: {
      type: 'boolean'
    },
    return: {
      type: 'boolean'
    }
  },
  additionalProperties: false
}];

},{}],24:[function(require,module,exports){
/**
 * @fileoverview Utility functions for React components detection
 * @author Yannick Croissant
 */
'use strict';

var util = require('util');

var DEFAULT_COMPONENT_NAME = 'eslintReactComponent';

/**
 * Detect if we are in a React Component
 * A React component is defined has an object/class with a property "render"
 * that return a JSXElement or null/false
 * @param {Object} context The current rule context.
 * @param {ASTNode} node The AST node being checked.
 * @returns {Boolean} True if we are in a React Component, false if not.
 */
function isReactComponent(context, node) {
  if (node.type !== 'ReturnStatement') {
    throw new Error('React Component detection must be done from a ReturnStatement ASTNode');
  }

  var scope = context.getScope();
  var isComponentRender =
    node.argument &&
    node.argument.type === 'JSXElement' &&
    scope.block.parent.key && scope.block.parent.key.name === 'render'
  ;
  var isEmptyComponentRender =
    node.argument &&
    node.argument.type === 'Literal' && (node.argument.value === null || node.argument.value === false) &&
    scope.block.parent.key && scope.block.parent.key.name === 'render'
  ;

  return Boolean(isEmptyComponentRender || isComponentRender);
}

/**
 * Detect if the node is a component definition
 * @param {ASTNode} node The AST node being checked.
 * @returns {Boolean} True the node is a component definition, false if not.
 */
function isComponentDefinition(node) {
  var isES6Component = node.type === 'ClassDeclaration';
  var isES5Component = Boolean(
    node.type === 'ObjectExpression' &&
    node.parent &&
    node.parent.callee &&
    node.parent.callee.object &&
    node.parent.callee.property &&
    node.parent.callee.object.name === 'React' &&
    node.parent.callee.property.name === 'createClass'
  );
  return isES5Component || isES6Component;
}

/**
 * Get the React component ASTNode from any child ASTNode
 * @param {Object} context The current rule context.
 * @param {ASTNode} node The AST node being checked.
 * @returns {ASTNode} The ASTNode of the React component.
 */
function getNode(context, node) {
  var componentNode = null;
  var ancestors = context.getAncestors().reverse();

  ancestors.unshift(node);

  for (var i = 0, j = ancestors.length; i < j; i++) {
    if (isComponentDefinition(ancestors[i])) {
      componentNode = ancestors[i];
      break;
    }
  }

  return componentNode;
}

/**
 * Get the identifiers of a React component ASTNode
 * @param {ASTNode} node The React component ASTNode being checked.
 * @returns {Object} The component identifiers.
 */
function getIdentifiers(node) {
  var name = node.id && node.id.name || DEFAULT_COMPONENT_NAME;
  var id = name + ':' + node.loc.start.line + ':' + node.loc.start.column;

  return {
    id: id,
    name: name
  };
}

/**
 * Store a React component list
 * @constructor
 */
function List() {
  this._list = {};
  this._length = 0;
}

/**
 * Find a component in the list by his node or one of his child node
 * @param {Object} context The current rule context.
 * @param {ASTNode} node The node to find.
 * @returns {Object|null} The component if it is found, null if not.
 */
List.prototype.getByNode = function(context, node) {
  var componentNode = getNode(context, node);
  if (!componentNode) {
    return null;
  }
  var identifiers = getIdentifiers(componentNode);

  return this._list[identifiers.id] || null;
};

/**
 * Find a component in the list by his name
 * @param {String} name Name of the component to find.
 * @returns {Object|null} The component if it is found, null if not.
 */
List.prototype.getByName = function(name) {
  for (var component in this._list) {
    if (this._list.hasOwnProperty(component) && this._list[component].name === name) {
      return this._list[component];
    }
  }
  return null;
};

/**
 * Return the component list
 * @returns {Object} The component list.
 */
List.prototype.getList = function() {
  return this._list;
};

/**
 * Add/update a component in the list
 * @param {Object} context The current rule context.
 * @param {ASTNode} node The node to add.
 * @param {Object} customProperties Additional properties to add to the component.
 * @returns {Object} The added component.
 */
List.prototype.set = function(context, node, customProperties) {
  var componentNode = getNode(context, node);
  if (!componentNode) {
    return null;
  }
  var identifiers = getIdentifiers(componentNode);

  var component = util._extend({
    name: identifiers.name,
    node: componentNode
  }, customProperties || {});

  if (!this._list[identifiers.id]) {
    this._length++;
  }

  this._list[identifiers.id] = util._extend(this._list[identifiers.id] || {}, component);

  return component;
};

/**
 * Remove a component from the list
 * @param {Object} context The current rule context.
 * @param {ASTNode} node The node to remove.
 */
List.prototype.remove = function(context, node) {
  var componentNode = getNode(context, node);
  if (!componentNode) {
    return null;
  }
  var identifiers = getIdentifiers(componentNode);

  if (!this._list[identifiers.id]) {
    return null;
  }

  delete this._list[identifiers.id];
  this._length--;

  return null;
};

/**
 * Return the component list length
 * @returns {Number} The component list length.
 */
List.prototype.count = function() {
  return this._length;
};

module.exports = {
  DEFAULT_COMPONENT_NAME: DEFAULT_COMPONENT_NAME,
  isReactComponent: isReactComponent,
  getNode: getNode,
  isComponentDefinition: isComponentDefinition,
  getIdentifiers: getIdentifiers,
  List: List
};

},{"util":5}],25:[function(require,module,exports){
/**
 * @fileoverview Utility functions for React components detection
 * @author Yannick Croissant
 */
'use strict';

/**
 * Record that a particular variable has been used in code
 *
 * @param {String} name The name of the variable to mark as used.
 * @returns {Boolean} True if the variable was found and marked as used, false if not.
 */
function markVariableAsUsed(context, name) {
  var scope = context.getScope();
  var variables;
  var i;
  var len;
  var found = false;

  // Special Node.js scope means we need to start one level deeper
  if (scope.type === 'global') {
    while (scope.childScopes.length) {
      scope = scope.childScopes[0];
    }
  }

  do {
    variables = scope.variables;
    for (i = 0, len = variables.length; i < len; i++) {
      if (variables[i].name === name) {
        variables[i].eslintUsed = true;
        found = true;
      }
    }
    scope = scope.upper;
  } while (scope);

  return found;
}

/**
 * Search a particular variable in a list
 * @param {Array} variables The variables list.
 * @param {Array} name The name of the variable to search.
 * @returns {Boolean} True if the variable was found, false if not.
 */
function findVariable(variables, name) {
  var i;
  var len;

  for (i = 0, len = variables.length; i < len; i++) {
    if (variables[i].name === name) {
      return true;
    }
  }

  return false;
}

/**
 * List all variable in a given scope
 *
 * Contain a patch for babel-eslint to avoid https://github.com/babel/babel-eslint/issues/21
 *
 * @param {Object} context The current rule context.
 * @param {Array} name The name of the variable to search.
 * @returns {Boolean} True if the variable was found, false if not.
 */
function variablesInScope(context) {
  var scope = context.getScope();
  var variables = scope.variables;

  while (scope.type !== 'global') {
    scope = scope.upper;
    variables = scope.variables.concat(variables);
  }
  if (scope.childScopes.length) {
    variables = scope.childScopes[0].variables.concat(variables);
    if (scope.childScopes[0].childScopes.length) {
      variables = scope.childScopes[0].childScopes[0].variables.concat(variables);
    }
  }

  return variables;
}

module.exports = {
  findVariable: findVariable,
  variablesInScope: variablesInScope,
  markVariableAsUsed: markVariableAsUsed
};

},{}],"eslint-plugin-react":[function(require,module,exports){
'use strict';

module.exports = {
  rules: {
    'jsx-uses-react': require('./lib/rules/jsx-uses-react'),
    'no-multi-comp': require('./lib/rules/no-multi-comp'),
    'prop-types': require('./lib/rules/prop-types'),
    'display-name': require('./lib/rules/display-name'),
    'wrap-multilines': require('./lib/rules/wrap-multilines'),
    'self-closing-comp': require('./lib/rules/self-closing-comp'),
    'no-did-mount-set-state': require('./lib/rules/no-did-mount-set-state'),
    'no-did-update-set-state': require('./lib/rules/no-did-update-set-state'),
    'react-in-jsx-scope': require('./lib/rules/react-in-jsx-scope'),
    'jsx-uses-vars': require('./lib/rules/jsx-uses-vars'),
    'jsx-no-undef': require('./lib/rules/jsx-no-undef'),
    'jsx-quotes': require('./lib/rules/jsx-quotes'),
    'no-unknown-property': require('./lib/rules/no-unknown-property'),
    'jsx-sort-props': require('./lib/rules/jsx-sort-props'),
    'jsx-sort-prop-types': require('./lib/rules/jsx-sort-prop-types'),
    'jsx-boolean-value': require('./lib/rules/jsx-boolean-value'),
    'sort-comp': require('./lib/rules/sort-comp'),
    'require-extension': require('./lib/rules/require-extension')
  },
  rulesConfig: {
    'jsx-uses-react': 0,
    'no-multi-comp': 0,
    'prop-types': 0,
    'display-name': 0,
    'wrap-multilines': 0,
    'self-closing-comp': 0,
    'no-did-mount-set-state': 0,
    'no-did-update-set-state': 0,
    'react-in-jsx-scope': 0,
    'jsx-uses-vars': 1,
    'jsx-no-undef': 0,
    'jsx-quotes': 0,
    'no-unknown-property': 0,
    'jsx-sort-props': 0,
    'jsx-sort-prop-types': 0,
    'jsx-boolean-value': 0,
    'sort-comp': 0,
    'require-extension': 0
  }
};

},{"./lib/rules/display-name":6,"./lib/rules/jsx-boolean-value":7,"./lib/rules/jsx-no-undef":8,"./lib/rules/jsx-quotes":9,"./lib/rules/jsx-sort-prop-types":10,"./lib/rules/jsx-sort-props":11,"./lib/rules/jsx-uses-react":12,"./lib/rules/jsx-uses-vars":13,"./lib/rules/no-did-mount-set-state":14,"./lib/rules/no-did-update-set-state":15,"./lib/rules/no-multi-comp":16,"./lib/rules/no-unknown-property":17,"./lib/rules/prop-types":18,"./lib/rules/react-in-jsx-scope":19,"./lib/rules/require-extension":20,"./lib/rules/self-closing-comp":21,"./lib/rules/sort-comp":22,"./lib/rules/wrap-multilines":23}]},{},[]);
