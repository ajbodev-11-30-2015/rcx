
o = {}

o.ls = 
  name: 'ls', engines: ['LiveScript'], outputFormat: 'js'
  sync: (str, options) -> 
    options.bare = true
    @cache(options) || @cache options, (@engine.compile str, options)

o.__ = 
  name: '__', engines: ['.'], outputFormat: 'html'
  sync: (str) -> str

o.jadejs = 
  name: 'jadejs', engines: ['jade'], outputFormat: 'js'
  sync: (str, options) -> 
    options.bare = true
    @cache(options) || @cache options, (@engine.compileClient str, options)

if typeof module == 'object' then module.exports = o