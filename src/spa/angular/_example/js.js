
var app = {};

app.angular = angular.module('app', ['ui.router']);

app.routes = function($stateProvider, $urlRouterProvider){
  $stateProvider
  .state('home', {
    url: '/',
    template: '<p>Home</p>'
  })
  .state('jobtitle', {
    url: '/som',
    template: '<p>Som</p>'
  });
};

app.angular.config(app.routes);