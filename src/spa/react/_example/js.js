
window.r = React.createElement;

var Home = React.createClass({
  render: function(){ return r('div', {}, 'Home'); }
});

var Som = React.createClass({
  render: function(){ return r('div', {}, 'Som'); }
});

var init = function(){
  route = ReactRouter.Route;
  var main = React.createClass({
    render: function(){return r(ReactRouter.RouteHandler, null);}
  });
  routes = r(route, {name: 'app',path: '/', handler: main}, 
    r(route, {path: '', handler: Home }), 
    r(route, {path: 'som', handler: Som })
  );
  ReactRouter.run(routes, function(Handler, state){
    React.render(r(Handler, {params: state.params}), document.getElementById('app'));
  });
};

init(); 