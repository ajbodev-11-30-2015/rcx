
xml2js = require 'xml2js'

app = do ->
  # Init
  o = 
    editors: {} 
    exports: {}
    runs:    {}
  
  run__auto              = document.getElementById 'run__auto'
  save__auto             = document.getElementById 'save__auto'
  run__delay             = 0
  save__delay            = 0
  
  _init = !->
    # Local Storage
    localforage.setDriver localforage.LOCALSTORAGE
    # Editors
    o.editors = 
      input__format:  _editor id: 'input__format',  mode: 'application/json', lint: true
      output__format: _editor id: 'output__format', mode: 'text/x-yaml', lint: false
    # Exports
    o.exports =
      input__format:   _exporter input: 'input__format',  type: 'application/json;charset=utf-8', ext: 'json'
      output__format:  _exporter input: 'output__format', type: 'text/x-yaml;charset=utf-8', ext: 'yaml'
    # Linters
    o.lints = 
      input__format:   _linter input: 'input__format',  elem: 'lint__input__format'
      output__format:  _linter input: 'output__format', elem: 'lint__output__format'
    # On change, run and/or save
    for let i, j of o.editors
      o.editors[i].on 'change', !->
        clearTimeout run__delay; clearTimeout save__delay
        if run__auto.checked  then run__delay  = setTimeout (!-> o.runs[i]!), 300
        if save__auto.checked then save__delay = setTimeout (!-> o.save!), 300
        console.clear!
  
  # Iframe
  _iframeSrc = ->
    previewFrame = document.getElementById 'output__iframe'
    preview =  previewFrame.contentDocument ||  previewFrame.contentWindow.document;
    '<!DOCTYPE html>' + preview .getElementsByTagName('html')[0] .outerHTML
    
  # Editors
  _editor = (_o) -> 
    CodeMirror.fromTextArea (document.getElementById _o.id), do
      mode: _o.mode
      theme: 'blackboard'
      gutters: ['CodeMirror-lint-markers']
      tabMode: 'indent'
      lineNumbers: true
      indentUnit: 2
      lineWrapping: true
      lint: _o.lint
  
  # Export
  _exporter = (_o) -> !->
    if _o.input then _input = o.editors[_o.input].getValue!
    else _input = _o.src!
    blob = new Blob([_input], type: _o.type)
    date = (moment! .format 'MMM[]Do-h[]mm[]a')
    saveAs blob, 'rcx-format-' + date + '.' + _o.ext
  
  # Lint
  _linter = (_o) -> !->
    lint = document.getElementById _o.elem
    if lint.checked then o.editors[_o.input].setOption 'lint', true
    else o.editors[_o.input].setOption 'lint', false

  # Mode   
  o.mode = (sel, format) !->
    config =
      json: lint: true,  mode: 'application/json', ext: 'json'
      yaml: lint: true, mode: 'text/x-yaml', ext: 'yaml'
      xml:  lint: false, mode: 'text/xml', ext: 'xml'
    mode = sel.value
    # Editor
    o.editors[format].setOption 'mode', config[mode].mode
    # Lint : Disable/Enable
    o.editors[format].setOption 'lint', false
    lint = document.getElementById 'lint__' + format
    lint.checked = false
    lint.disabled = !config[mode].lint
    # Export
    o.exports[format] = _exporter do
      input: format
      type: config[mode].mode + ';charset=utf-8'
      ext: config[mode].ext
  
  # Runs
  o.runs = 
    input__format:  !-> 
      # Matcher
      _modeInputFormat  = document.getElementById 'mode__input__format'
      _modeOutputFormat = document.getElementById 'mode__output__format'
      #alert _modeInputFormat.selectedIndex + ' ' + _modeOutputFormat.selectedIndex
      #if _modeInputFormat.selectedIndex != _modeOutputFormat.selectedIndex then alert 4
      _converter = 
        '--' : !-> # JSON -> JSON | YAML -> YAML | XML  -> XML
          app.editors['output__format'].getDoc! .setValue do
            o.editors['input__format'].getValue!
        '00' : !-> _converter['--']!
        '11' : !-> _converter['--']!
        '22' : !-> _converter['--']!
        '01' : !-> # JSON -> YAML
          _yaml = YAML.stringify (JSON.parse o.editors['input__format'].getValue!)
          app.editors['output__format'].getDoc! .setValue _yaml
        '02' : !-> # JSON -> XML
          _xml = new xml2js.Builder! .buildObject (JSON.parse o.editors['input__format'].getValue!)
          app.editors['output__format'].getDoc! .setValue _xml
        '10' : !-> # YAML -> JSON
          _js = YAML.parse o.editors['input__format'].getValue!
          app.editors['output__format'].getDoc! .setValue (JSON.stringify _js, null, 2)
        '12' : !-> # ! YAML -> XML
          _js = YAML.parse o.editors['input__format'].getValue!
          _xml = new xml2js.Builder! .buildObject _js
          app.editors['output__format'].getDoc! .setValue _xml
        '20' : !-> # XML -> JSON
          xml2js.parseString o.editors['input__format'].getValue!, (err, result) !->
            app.editors['output__format'].getDoc! .setValue (JSON.stringify result, null, 2)
        '21' : !-> # ! XML -> YAML
          xml2js.parseString o.editors['input__format'].getValue!, (err, result) !->
            _yaml = YAML.stringify result
            app.editors['output__format'].getDoc! .setValue _yaml
      # Convert
      _converter[_modeInputFormat.selectedIndex + '' + _modeOutputFormat.selectedIndex]!
    output__format: !-> 
  
  # Error
  o.error = (e) !->
    document.getElementById 'output__error' .innerHTML = e
    console.log e
      
  # Import
  o.importer = (input, editor) !->
    reader = new FileReader!
    reader.readAsText input.files[0]
    reader.onload = (e) !->
      o.editors[editor].getDoc! .setValue e.target.result
  
  # Save
  o.save = !->
    for let i, j of o.editors
      localforage.setItem 'rcx/format/' + i, o.editors[i].getValue!, (e, v) -> 
        console.log 'Save ' + i
   
  # Load   
  o.load = !->
    for let i, j of o.editors
      localforage.getItem 'rcx/format/' + i, (e, v) ->
        o.editors[i].getDoc! .setValue v
  
  # Alternative Layout (On/Off)
  o.altLayout = !->
    alt__layout = document.getElementById 'alt__layout'
    _editors = {}
    for i, j of o.editors
      _editors[i] = o.editors[i].getValue!
    if alt__layout.checked then $ '#app__layout' .html dmt['layout-alt']
    else $ '#app__layout' .html dmt['layout-default']
    setTimeout !-> 
      _init!
      for i, j of o.editors
        o.editors[i].getDoc! .setValue _editors[i]
    , 300 
  
  # Init & Return
  _init!
  o

# Defaults
['input__format'].map (el, i) !-> 
  app.editors[el].getDoc! .setValue dmt[el]