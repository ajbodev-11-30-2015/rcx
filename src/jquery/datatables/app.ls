
app = do ->
  # Init
  o = 
    editors: {} 
    exports: {}
    runs:    {}
  
  run__auto              = document.getElementById 'run__auto'
  save__auto             = document.getElementById 'save__auto'
  run__delay             = 0
  save__delay            = 0
  
  _init = !->
    # Local Storage
    localforage.setDriver localforage.LOCALSTORAGE
    # Editors
    o.editors = 
      input__html: _editor id: 'input__html',  mode: 'text/html',        lint: false
      input__data: _editor id: 'input__data',  mode: 'application/json', lint: true
      input__js:   _editor id: 'input__js',    mode: 'text/javascript',  lint: true
    # Exports
    o.exports =
      input__html:    _exporter input: 'input__html', type: 'text/html;charset=utf-8',        ext: 'html'
      input__data:    _exporter input: 'input__data', type: 'application/json;charset=utf-8', ext: 'json'
      input__js:      _exporter input: 'input__js',   type: 'text/javascript;charset=utf-8',  ext: 'js'
      output__iframe: _exporter src: _iframeSrc,      type: 'text/html;charset=utf-8',        ext: 'html'
    # Linters
    o.lints = 
      input__data: _linter input: 'input__data', elem: 'lint__input__data'
      input__js:   _linter input: 'input__js',   elem: 'lint__input__js'
    # On change, run and/or save
    for let i, j of o.editors
      o.editors[i].on 'change', !->
        clearTimeout run__delay; clearTimeout save__delay
        if run__auto.checked  then run__delay  = setTimeout (!-> o.runs[i]!), 300
        if save__auto.checked then save__delay = setTimeout (!-> o.save!), 300
        console.clear!
  
  # Iframe
  _iframeSrc = ->
    previewFrame = document.getElementById 'output__iframe'
    preview =  previewFrame.contentDocument ||  previewFrame.contentWindow.document;
    '<!DOCTYPE html>' + preview .getElementsByTagName('html')[0] .outerHTML
    
  # Editors
  _editor = (_o) -> 
    CodeMirror.fromTextArea (document.getElementById _o.id), do
      mode: _o.mode
      theme: 'blackboard'
      gutters: ['CodeMirror-lint-markers']
      tabMode: 'indent'
      lineNumbers: true
      indentUnit: 2
      lineWrapping: true
      lint: _o.lint
  
  # Export
  _exporter = (_o) -> !->
    if _o.input then _input = o.editors[_o.input].getValue!
    else _input = _o.src!
    blob = new Blob([_input], type: _o.type)
    date = (moment! .format 'MMM[]Do-h[]mm[]a')
    saveAs blob, 'rcx-jquery-datatables-' + date + '.' + _o.ext #'try.html'
  
  # Lint
  _linter = (_o) -> !->
    lint = document.getElementById _o.elem
    if lint.checked then o.editors[_o.input].setOption 'lint', true
    else o.editors[_o.input].setOption 'lint', false
  
  # Runs
  o.runs = 
    input__html: !-> 
      _html = o.editors['input__html'].getValue!
      _data = o.editors['input__data'].getValue!
      _js   = o.editors['input__js'].getValue!
      previewFrame = document.getElementById 'output__iframe'
      preview =  previewFrame.contentDocument || previewFrame.contentWindow.document
      previewWindow = previewFrame.contentWindow || previewFrame
      preview.open!
      preview.write _html
      # CSS (DataTables)
      style = preview.createElement 'style'
      style.textContent = dmt['jquery-datatables-css']
      preview.head .appendChild style
      # Data
      data = preview.createElement 'script'
      data.textContent = 'var data = ' + _data + ';'
      preview.body .appendChild data
      # JS
      ['jquery' 'jquery-datatables-js'].map (el, i) !->
        script = preview.createElement 'script'
        script.textContent = dmt[el]
        preview.body .appendChild script
      script = preview.createElement 'script'
      script.textContent = 'try {' + _js + '} catch(e) { window.parent.app.error(e); }'
      preview.body .appendChild script
      preview.close!
    input__data: !-> o.runs.input__html!
    input__js:  !-> o.runs.input__html!
  
  # Error
  o.error = (e) !->
    document.getElementById 'output__error' .innerHTML = e
    console.log e
      
  # Import
  o.importer = (input, editor) !->
    reader = new FileReader!
    reader.readAsText input.files[0]
    reader.onload = (e) !->
      o.editors[editor].getDoc! .setValue e.target.result
  
  # Save
  o.save = !->
    for let i, j of o.editors
      localforage.setItem 'rcx/jquery/datatables/' + i, o.editors[i].getValue!, (e, v) -> 
        console.log 'Save ' + i
   
  # Load   
  o.load = !->
    for let i, j of o.editors
      localforage.getItem 'rcx/jquery/datatables/' + i, (e, v) ->
        o.editors[i].getDoc! .setValue v
  
  # Alternative Layout (On/Off)
  o.altLayout = !->
    alt__layout = document.getElementById 'alt__layout'
    _editors = {}
    for i, j of o.editors
      _editors[i] = o.editors[i].getValue!
    if alt__layout.checked then $ '#app__layout' .html dmt['layout-alt']
    else $ '#app__layout' .html dmt['layout-default']
    setTimeout !-> 
      _init!
      for i, j of o.editors
        o.editors[i].getDoc! .setValue _editors[i]
    , 300 
  
  # Init & Return
  _init!
  o

# Defaults
['input__html' 'input__data' 'input__js'].map (el, i) !-> 
  app.editors[el].getDoc! .setValue dmt[el]