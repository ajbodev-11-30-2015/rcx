
db = new SQL.Database!

knex = Knex client: 'sqlite3'

app = do ->
  # Init
  o = 
    editors: {} 
    exports: {}
    runs:    {}
  
  run__auto              = document.getElementById 'run__auto'
  save__auto             = document.getElementById 'save__auto'
  run__delay             = 0
  save__delay            = 0
  
  _init = !->
    # Local Storage
    localforage.setDriver localforage.LOCALSTORAGE
    # Editors
    o.editors = 
      input__sql:     _editor id: 'input__sql',     mode: 'text/x-sql',      lint: false
      input__builder: _editor id: 'input__builder', mode: 'text/javascript', lint: true
    # Exports
    o.exports =
      input__sql:     _exporter input: 'input__sql',     type: 'text/x-sql;charset=utf-8',      ext: 'sql'
      input__builder: _exporter input: 'input__builder', type: 'text/javascript;charset=utf-8', ext: 'js'
      output__iframe: _exporter src: _iframeSrc,         type: 'text/html;charset=utf-8',       ext: 'html'
    # On change, run and/or save
    for let i, j of o.editors
      o.editors[i].on 'change', !->
        clearTimeout run__delay; clearTimeout save__delay
        if run__auto.checked  then run__delay  = setTimeout (!-> o.runs[i]!), 300
        if save__auto.checked then save__delay = setTimeout (!-> o.save!), 300
        console.clear!
  
  # Iframe
  _iframeSrc = ->
    previewFrame = document.getElementById 'output__iframe'
    preview =  previewFrame.contentDocument ||  previewFrame.contentWindow.document;
    '<!DOCTYPE html>' + preview .getElementsByTagName('html')[0] .outerHTML
    
  # Editors
  _editor = (_o) -> 
    CodeMirror.fromTextArea (document.getElementById _o.id), do
      mode: _o.mode
      theme: 'blackboard'
      gutters: ['CodeMirror-lint-markers']
      tabMode: 'indent'
      lineNumbers: true
      indentUnit: 2
      lineWrapping: true
      lint: _o.lint
  
  # Export
  _exporter = (_o) -> !->
    if _o.input then _input = o.editors[_o.input].getValue!
    else _input = _o.src!
    blob = new Blob([_input], type: _o.type)
    date = (moment! .format 'MMM[]Do-h[]mm[]a')
    saveAs blob, 'rcx-db-sql-' + date + '.' + _o.ext
  
  # Lint
  _linter = (_o) -> !->
    lint = document.getElementById _o.elem
    if lint.checked then o.editors[_o.input].setOption 'lint', true
    else o.editors[_o.input].setOption 'lint', false
  
  # Runs
  o.runs = 
    input__sql: !-> 
      _html = '<!DOCTYPE html><html><head><style></style></head><body>'
      _sql  = db.exec o.editors['input__sql'].getValue!
      console.log _sql
      
      for let i of _sql
        _html += '<table><thead><tr>'
        for let j of _sql[i].columns
          _html += '<th>' + _sql[i].columns[j] + '</th>'
        _html += '</tr></thead><tbody>'
        for let j of _sql[i].values
          _html += '<tr>'
          for let k of _sql[i].values[j]
            _html += '<td>' + _sql[i].values[j][k] + '</td>'
          _html += '</tr>'
        _html += '</tbody></table>'
      _html += '</body></html>'
      
      output__iframe         = document.getElementById 'output__iframe'
      output__iframeDocument = output__iframe.contentDocument || output__iframe.contentWindow.document
      output__iframeDocument.open!
      output__iframeDocument.write _html
      output__iframeDocument.close!
    input__builder: !-> 
      _html = '<!DOCTYPE html><html><head><style></style></head><body></body></html>'
      _js  = o.editors['input__builder'].getValue!
      
      output__iframe         = document.getElementById 'output__iframe__builder'
      output__iframeDocument = output__iframe.contentDocument || output__iframe.contentWindow.document
      output__iframeDocument.open!
      output__iframeDocument.write _html
      script = output__iframeDocument.createElement 'script'
      script.textContent = 'var knex = window.parent.knex; ' + 
        'var sql = window.parent.app.sql; ' + 
        _js
      output__iframeDocument.body .appendChild script
      output__iframeDocument.close!
   
  # Special (Builder -> SQL)
  o.sql = (_sql) !->
    o.editors['input__sql'].getDoc! .setValue do
      o.editors['input__sql'].getValue! + '\n' + 
      _sql + ''
  
  # Error
  o.error = (e) !->
    document.getElementById 'output__error' .innerHTML = e
    console.log e
      
  # Import
  o.importer = (input, editor) !->
    reader = new FileReader!
    reader.readAsText input.files[0]
    reader.onload = (e) !->
      o.editors[editor].getDoc! .setValue e.target.result
  
  # Save
  o.save = !->
    for let i, j of o.editors
      localforage.setItem 'rcx/db/sql/' + i, o.editors[i].getValue!, (e, v) -> 
        console.log 'Save ' + i
   
  # Load   
  o.load = !->
    for let i, j of o.editors
      localforage.getItem 'rcx/db/sql/' + i, (e, v) ->
        o.editors[i].getDoc! .setValue v
  
  # Alternative Layout (On/Off)
  o.altLayout = !->
    alt__layout = document.getElementById 'alt__layout'
    _editors = {}
    for i, j of o.editors
      _editors[i] = o.editors[i].getValue!
    if alt__layout.checked then $ '#app__layout' .html dmt['layout-alt']
    else $ '#app__layout' .html dmt['layout-default']
    setTimeout !-> 
      _init!
      for i, j of o.editors
        o.editors[i].getDoc! .setValue _editors[i]
    , 300 
  
  # Init & Return
  _init!
  o

# Defaults
['input__sql' 'input__builder'].map (el, i) !-> 
  app.editors[el].getDoc! .setValue dmt[el]