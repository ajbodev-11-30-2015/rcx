
mixin Controller(o)
  :__
    #{''}
    class #{o.group}::!{window._.toTitleCase(o.route)}Controller < ApplicationController
      def list
        entities = #{o.group}::#{o.model}.all
        render json: entities
      end
      def edit
        entity = #{o.group}::#{o.model}.find_by(id: params[:id])
        render json: entity
      end
      def add
        entity = #{o.group}::#{o.model}.create(
  - /* FOR */ for (var i in o.column) {
  :__
    #{''}
          #{i}: params[:#{i}]
  - /* END */ }
  :__
    #{''}
        )
        render :text => 'Added'
      end
      def update
        entity = #{o.group}::#{o.model}.find_by(id: params[:id])
  - /* FOR */ for (var i in o.column) {
  :__
    #{''}
        entity.#{i} = params[:#{i}]
  - /* END */ }
  :__
    #{''}
        entity.save
        render :text => 'Updated'
      end
      def delete
        entity = #{o.group}::#{o.model}.find_by(id: params[:id])
        entity.destroy
        render :text => 'Deleted'
      end
    end