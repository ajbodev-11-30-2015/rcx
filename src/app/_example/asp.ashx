
mixin Controller(o)
  :__
    #{""}
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Entity;
    using System.Linq;
    using System.Net;
    using System.Web;
    using System.Web.Mvc;
    using Newtonsoft.Json;
    using MaroonHRM.DAL;
    using MaroonHRM.Models.#{o.group};

    using System.Diagnostics;

    namespace MaroonHRM.Controllers.#{o.group}
    {
        public class #{o.model}Controller : Controller
        {
            private MaroonHRMContext db = new MaroonHRMContext();

            [HttpGet]
            public ActionResult Edit(int? id)
            {
                #{o.model} entity = db.#{o.entity}.Find(id);
                return Content(JsonConvert.SerializeObject(entity));
            }

            [HttpGet]
            public ActionResult List()
            {
                return Content(JsonConvert.SerializeObject(db.#{o.entity}));
            }

            [HttpPost]
            public ActionResult Add([Bind(Include = "#{window._.colObjToStr(o.column)}")] #{o.model} entity)
            {
                db.#{o.entity}.Add(entity);
                db.SaveChanges();
                return Content("Added");
            }

            [HttpPost]
            public ActionResult Update([Bind(Include = "id,#{window._.colObjToStr(o.column)}")] #{o.model} entity)
            {
                db.Entry(entity).State = EntityState.Modified;
                db.SaveChanges();
                return Content("Updated");
            }

            [HttpPost]
            public ActionResult Delete(int id)
            {
                #{o.model} entity = db.#{o.entity}.Find(id);
                db.JobTitles.Remove(entity);
                db.SaveChanges();
                return Content("Deleted");
            }
        }
    }
