
mixin Controller(o)
  :__
    #{''}
    <?php namespace App\Http\Controllers#{'\\'}#{o.group};

    use Illuminate\Support\Facades\Request;
    use App\Http\Controllers\Controller;
    use App\Entity#{'\\'}#{o.group}#{'\\'}#{o.model};

    class #{o.model}Controller extends Controller {

        public function __construct()
        {
            #$this->middleware('auth');
        }

        public function getEdit($id)
        {
            $entity = #{o.model}::find($id);
            return json_encode($job);
        }

        public function getList()
        {
            $entities = #{o.model}::all();
            return json_encode($entities);
        }

        public function postAdd()
        {
            $entity = new #{o.model}
  - /* FOR */ for (var i in o.column) {
  :__
    #{''}
            $entity->#{i} = Request::input('#{i}');
  - /* END */ }
  :__
    #{''}
            $entity->save();
            return 'Added';
        }

        public function postUpdate()
        {
            $entity = #{o.model}::find(Request::input('id'));
  - /* FOR */ for (var i in o.column) {
  :__
    #{''}
            $entity->#{i} = Request::input('#{i}');
  - /* END */ }
  :__
    #{''}
            $entity->save();
            return 'Update';
        }

        public function postDelete()
        {
            $entity = #{o.model}::find(Request::input('id'));
            $entity->delete();
            return 'Delete';
        }

    }