
mixin Controller(o)
  :__
    #{''}
    from flask import jsonify
    from maroonhrm import app, db, render_template, request
    from maroonhrm.models.#{o.group.toLowerCase()}.#{o.model.toLowerCase()} import #{o.model}

    @app.route('/#{o.route}/list', methods=['GET'])
    def list():
        entities = []
        for entity in #{o.model}.query.all():  
            entities.append(entity.toJSON())
        return "[" + ",".join(entities) + "]"

    @app.route('/#{o.route}/edit/<id>', methods=['GET'])
    def edit(id=None):
        entity = #{o.model}.query.get(id)
        return entity.toJSON()

    @app.route('/#{o.route}/add', methods=['POST'])
    def add():
        entity = #{o.model}()
  - /* FOR */ for (var i in o.column) {
  :__
    #{''}
        entity.#{i} = request.form['#{i}']
  - /* END */ }
  :__
    #{''}
        db.session.add(entity)
        db.session.commit()
        return 'Added'

    @app.route('/#{o.route}/update', methods=['POST'])
    def update():
        entity = #{o.model}.query.get(request.form['id'])
  - /* FOR */ for (var i in o.column) {
  :__
    #{''}
        entity.#{i} = request.form['#{i}']
  - /* END */ }
  :__
    #{''}
        db.session.commit()
        return 'Updated'


    @app.route('/#{o.route}/delete', methods=['POST'])
    def delete():
        entity = #{o.model}.query.get(request.form['id'])
        db.session.delete(entity)
        db.session.commit()
        return request.form['id']