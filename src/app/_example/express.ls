
mixin Controller(o)
  :__
    #{''}
    mongojs = require 'mongojs', db = use 'models/db'

    o = {}

    o.get = 
      'list': (req, res) !->
        _docs = []
        db['#{o.model.toLowerCase()}'].find (err, docs) !->
          res.send docs
      'edit/:id': (req, res) !->
        id = mongojs.ObjectId(req.params.id)
        db['#{o.model.toLowerCase()}'].findOne _id: id, (err, doc) !->
          res.send doc

    o.post = 
      'add': (req, res) !->
        add =
  - /* FOR */ for (var i in o.column) {
  :__
    #{''}
          #{i}: req.body.#{i}
  - /* END */ }
  :__
    #{''}
        db['#{o.model.toLowerCase()}'].save do
          add
          (err, doc) !-> 
            console.log doc
        res.send 'Added'
      'update': (req, res) !->
        id = mongojs.ObjectId(req.body.id)
        update =
  - /* FOR */ for (var i in o.column) {
  :__
    #{''}
          #{i}: req.body.#{i}
  - /* END */ }
  :__
    #{''}
        db['#{o.model.toLowerCase()}'].update do
          {_id: id}
          {$set: update}
          (err, doc) !-> 
            console.log doc
        res.send 'Updated'
      'delete': (req, res) !->
        id = mongojs.ObjectId(req.body.id)
        db['#{o.model.toLowerCase()}'].remove do
          {_id: id}
          (err, doc) !-> 
            console.log doc
        res.send 'Deleted'

    if typeof module == 'object' then module.exports = o  