
mixin Controller(o)
  :__
    #{''}
    package App.Controllers.#{o.group};

    import org.springframework.stereotype.Controller;
    import org.springframework.web.bind.annotation.RequestMapping;
    import org.springframework.web.bind.annotation.RequestMethod;
    import org.springframework.web.bind.annotation.RequestParam;
    import org.springframework.web.bind.annotation.ResponseBody;
    import org.springframework.web.bind.annotation.PathVariable;
    import org.springframework.beans.factory.annotation.Autowired;
    import java.util.StringJoiner;
    import App.Repositories.#{o.group}.#{o.model}Repository;
    import App.Models.#{o.group}.#{o.model};

    @Controller
    @RequestMapping("/#{o.route}")
    public class #{o.model}Controller {

        @Autowired
        #{o.model}Repository repository;

        @RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
        public @ResponseBody String getEdit(@PathVariable long id) {
            #{o.model} entity = repository.findOne(id);
            return entity.toJSON();
        }

        @RequestMapping(value="/list", method=RequestMethod.GET)
        public @ResponseBody String getList() {
            StringJoiner entities = new StringJoiner(",");
            for (#{o.model} entity : repository.findAll()) {
                entities.add(entity.toJSON());
            }
            return "[" + entities.toString() + "]";
        }

        @RequestMapping(value="/add", method=RequestMethod.POST)
        public @ResponseBody String postAdd(#{window._.colObjToStrTyp(o.column)}) {
            #{o.model} entity = new #{o.model}();
  - /* FOR */ for (var i in o.column) {
  :__
    #{''}
            entity.set#{window._.set(i)}(#{i});
  - /* END */ }
  :__
    #{''}
            repository.save(entity);
            return "Added";
        }

        @RequestMapping(value="/update", method=RequestMethod.POST)
        public @ResponseBody String postUpdate(long id, #{window._.colObjToStrTyp(o.column)}) {
            #{o.model} entity = repository.findOne(id);
  - /* FOR */ for (var i in o.column) {
  :__
    #{''}
            entity.set#{window._.set(i)}(#{i});
  - /* END */ }
  :__
    #{''}
            repository.save(entity);
            return "Updated " + id;
        }

        @RequestMapping(value="/delete", method=RequestMethod.POST)
        public @ResponseBody String postDelete(long id) {
            repository.delete(id);
            return "Deleted " + id;
        }

    }