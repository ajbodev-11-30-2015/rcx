
app = do ->
  # Init
  o = 
    editors: {} 
    exports: {}
    runs:    {}
  
  run__auto              = document.getElementById 'run__auto'
  save__auto             = document.getElementById 'save__auto'
  run__delay             = 0
  save__delay            = 0
  
  _init = !->
    # Local Storage
    localforage.setDriver localforage.LOCALSTORAGE
    # Editors
    o.editors = 
      input__jade:      _editor id: 'input__jade',      mode: 'text/x-jade',      lint: false
      input__framework: _editor id: 'input__framework', mode: 'text/x-java',      lint: false
      input__schema:    _editor id: 'input__schema',    mode: 'application/json', lint: true
      input__helpers:   _editor id: 'input__helpers',   mode: 'text/x-jade',      lint: false
      output__result:   _editor id: 'output__result',   mode: 'text/x-java',      lint: false
    # Exports
    o.exports =
      input__jade:      _exporter input: 'input__jade',      type: 'text/x-jade;charset=utf-8',      ext: 'jade'
      input__framework: _exporter input: 'input__framework', type: 'text/x-jade;charset=utf-8',      ext: 'jade'
      input__schema:    _exporter input: 'input__schema',    type: 'application/json;charset=utf-8', ext: 'json'
      input__helpers:   _exporter input: 'input__helpers',   type: 'text/x-jade;charset=utf-8',      ext: 'jade'
      output__result:   _exporter input: 'output__result',   type: 'text/x-java;charset=utf-8',      ext: 'java'
    # Linters
    # On change, run and/or save
    for let i, j of o.editors
      o.editors[i].on 'change', !->
        clearTimeout run__delay; clearTimeout save__delay
        if run__auto.checked  then run__delay  = setTimeout (!-> o.runs[i]!), 300
        if save__auto.checked then save__delay = setTimeout (!-> o.save!), 300
        console.clear!
  
  # Iframe
  _iframeSrc = ->
    previewFrame = document.getElementById 'output__iframe'
    preview =  previewFrame.contentDocument ||  previewFrame.contentWindow.document;
    '<!DOCTYPE html>' + preview .getElementsByTagName('html')[0] .outerHTML
    
  # Editors
  _editor = (_o) -> 
    CodeMirror.fromTextArea (document.getElementById _o.id), do
      mode: _o.mode
      theme: 'blackboard'
      gutters: ['CodeMirror-lint-markers']
      tabMode: 'indent'
      lineNumbers: true
      indentUnit: 2
      lineWrapping: true
      lint: _o.lint
  
  # Export
  _exporter = (_o) -> !->
    if _o.input then _input = o.editors[_o.input].getValue!
    else _input = _o.src!
    blob = new Blob([_input], type: _o.type)
    date = (moment! .format 'MMM[]Do-h[]mm[]a')
    saveAs blob, 'rcx-app-spring-' + date + '.' + _o.ext #'try.html'
  
  # Lint
  _linter = (_o) -> !->
    lint = document.getElementById _o.elem
    if lint.checked then o.editors[_o.input].setOption 'lint', true
    else o.editors[_o.input].setOption 'lint', false
  
  # Runs
  o.runs = 
    input__jade: !-> 
      _output = 
        o.editors['input__helpers'].getValue! + '\n' +
        o.editors['input__framework'].getValue! + '\n' +
        '\n- window._.schema = ' + JSON.stringify (JSON.parse o.editors['input__schema'].getValue!)
        o.editors['input__jade'].getValue!
      o.editors['output__result'].getDoc! .setValue do
        jade.render _output, pretty:true
    input__framework: !-> o.runs.input__jade!
    input__schema:    !-> o.runs.input__jade!
    input__helpers:   !-> o.runs.input__jade!
    output__result:   !-> 
  
  # Error
  o.error = (e) !->
    document.getElementById 'output__error' .innerHTML = e
    console.log e
      
  # Import
  o.importer = (input, editor) !->
    reader = new FileReader!
    reader.readAsText input.files[0]
    reader.onload = (e) !->
      o.editors[editor].getDoc! .setValue e.target.result
  
  # Save
  o.save = !->
    for let i, j of o.editors
      localforage.setItem 'rcx/app/spring/' + i, o.editors[i].getValue!, (e, v) -> 
        console.log 'Save ' + i
   
  # Load   
  o.load = !->
    for let i, j of o.editors
      localforage.getItem 'rcx/app/spring/' + i, (e, v) ->
        o.editors[i].getDoc! .setValue v
  
  # Alternative Layout (On/Off)
  o.altLayout = !->
    alt__layout = document.getElementById 'alt__layout'
    _editors = {}
    for i, j of o.editors
      _editors[i] = o.editors[i].getValue!
    if alt__layout.checked then $ '#app__layout' .html dmt['layout-alt']
    else $ '#app__layout' .html dmt['layout-default']
    setTimeout !-> 
      _init!
      for i, j of o.editors
        o.editors[i].getDoc! .setValue _editors[i]
    , 300 
  
  # Init & Return
  _init!
  o

# Defaults
['input__jade' 'input__framework' 'input__schema' 'input__helpers'].map (el, i) !-> 
  app.editors[el].getDoc! .setValue dmt[el]