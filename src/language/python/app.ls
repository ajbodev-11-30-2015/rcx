
# Python
alert Sk
  
app = do ->
  # Init
  o = 
    editors: {} 
    exports: {}
    runs:    {}
  
  run__auto              = document.getElementById 'run__auto'
  save__auto             = document.getElementById 'save__auto'
  run__delay             = 0
  save__delay            = 0
  
  _init = !->
    # Local Storage
    localforage.setDriver localforage.LOCALSTORAGE
    # Editors
    o.editors = 
      input__python: _editor id: 'input__python', mode: 'text/x-python', lint: false
    # Exports
    o.exports =
      input__python:  _exporter input: 'input__python', type: 'text/x-python;charset=utf-8', ext: 'py'
      output__iframe: _exporter src: _iframeSrc,        type: 'text/html;charset=utf-8',     ext: 'html'
    # Linters
    # On change, run and/or save
    for let i, j of o.editors
      o.editors[i].on 'change', !->
        clearTimeout run__delay; clearTimeout save__delay
        if run__auto.checked  then run__delay  = setTimeout (!-> o.runs[i]!), 300
        if save__auto.checked then save__delay = setTimeout (!-> o.save!), 300
        console.clear!
  
  # Iframe
  _iframeSrc = ->
    previewFrame = document.getElementById 'output__iframe'
    preview =  previewFrame.contentDocument ||  previewFrame.contentWindow.document;
    '<!DOCTYPE html>' + preview .getElementsByTagName('html')[0] .outerHTML
    
  # Editors
  _editor = (_o) -> 
    CodeMirror.fromTextArea (document.getElementById _o.id), do
      mode: _o.mode
      theme: 'blackboard'
      gutters: ['CodeMirror-lint-markers']
      tabMode: 'indent'
      lineNumbers: true
      indentUnit: 2
      lineWrapping: true
      lint: _o.lint
  
  # Export
  _exporter = (_o) -> !->
    if _o.input then _input = o.editors[_o.input].getValue!
    else _input = _o.src!
    blob = new Blob([_input], type: _o.type)
    date = (moment! .format 'MMM[]Do-h[]mm[]a')
    saveAs blob, 'rcx-lang-python-' + date + '.' + _o.ext
  
  # Lint
  _linter = (_o) -> !->
    lint = document.getElementById _o.elem
    if lint.checked then o.editors[_o.input].setOption 'lint', true
    else o.editors[_o.input].setOption 'lint', false
  
  # Runs
  o.runs = 
    input__python: !-> 
      _python = o.editors['input__python'].getValue!
      previewFrame = document.getElementById 'output__iframe'
      preview = previewFrame.contentDocument || previewFrame.contentWindow.document
      preview.open!
      preview.innerHTML = ''
      Sk.configure do
       output: (text) !-> preview.write text
       read: (x) ->
         if Sk.builtinFiles == undefined || Sk.builtinFiles["files"][x] == undefined
           throw "File not found: '" + x + "'"
         Sk.builtinFiles["files"][x]
       Sk.misceval.asyncToPromise ->
         Sk.importMainWithBody '<stdin>', false, _python, true
       .then do
         (mod) !-> console.log 'success'
         (err) !-> console.log err.toString!
      preview.close!
  
  # Error
  o.error = (e) !->
    document.getElementById 'output__error' .innerHTML = e
    console.log e
      
  # Import
  o.importer = (input, editor) !->
    reader = new FileReader!
    reader.readAsText input.files[0]
    reader.onload = (e) !->
      o.editors[editor].getDoc! .setValue e.target.result
  
  # Save
  o.save = !->
    for let i, j of o.editors
      localforage.setItem 'rcx/lang/python/' + i, o.editors[i].getValue!, (e, v) -> 
        console.log 'Save ' + i
   
  # Load   
  o.load = !->
    for let i, j of o.editors
      localforage.getItem 'rcx/lang/python/' + i, (e, v) ->
        o.editors[i].getDoc! .setValue v
  
  # Alternative Layout (On/Off)
  o.altLayout = !->
    alt__layout = document.getElementById 'alt__layout'
    _editors = {}
    for i, j of o.editors
      _editors[i] = o.editors[i].getValue!
    if alt__layout.checked then $ '#app__layout' .html dmt['layout-alt']
    else $ '#app__layout' .html dmt['layout-default']
    setTimeout !-> 
      _init!
      for i, j of o.editors
        o.editors[i].getDoc! .setValue _editors[i]
    , 300 
  
  # Init & Return
  _init!
  o

# Defaults
['input__python'].map (el, i) !-> 
  app.editors[el].getDoc! .setValue dmt[el]