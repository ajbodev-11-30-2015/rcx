
app = do ->
  # Init
  o = 
    editors: {} 
    exports: {}
    runs:    {}
  
  run__auto              = document.getElementById 'run__auto'
  save__auto             = document.getElementById 'save__auto'
  run__delay             = 0
  save__delay            = 0
  
  _init = !->
    # Local Storage
    localforage.setDriver localforage.LOCALSTORAGE
    # Editors
    o.editors = 
      input__remark: _editor id: 'input__remark', mode: 'text/x-markdown', lint: false
    # Exports
    o.exports =
      input__remark:  _exporter input: 'input__remark', type: 'text/x-markdown;charset=utf-8', ext: 'md'
      output__iframe: _exporter src: _iframeSrc,        type: 'text/html;charset=utf-8',       ext: 'html'
    # Linters
    # On change, run and/or save
    for let i, j of o.editors
      o.editors[i].on 'change', !->
        clearTimeout run__delay; clearTimeout save__delay
        if run__auto.checked  then run__delay  = setTimeout (!-> o.runs[i]!), 300
        if save__auto.checked then save__delay = setTimeout (!-> o.save!), 300
        console.clear!
  
  # Iframe
  _iframeSrc = ->
    previewFrame = document.getElementById 'output__iframe'
    preview =  previewFrame.contentDocument ||  previewFrame.contentWindow.document;
    '<!DOCTYPE html>' + preview .getElementsByTagName('html')[0] .outerHTML
    
  # Editors
  _editor = (_o) -> 
    CodeMirror.fromTextArea (document.getElementById _o.id), do
      mode: _o.mode
      theme: 'blackboard'
      gutters: ['CodeMirror-lint-markers']
      tabMode: 'indent'
      lineNumbers: true
      indentUnit: 2
      lineWrapping: true
      lint: _o.lint
  
  # Export
  _exporter = (_o) -> !->
    if _o.input then _input = o.editors[_o.input].getValue!
    else _input = _o.src!
    blob = new Blob([_input], type: _o.type)
    date = (moment! .format 'MMM[]Do-h[]mm[]a')
    saveAs blob, 'rcx-others-remark-' + date + '.' + _o.ext #'try.html'
  
  # Lint
  _linter = (_o) -> !->
    lint = document.getElementById _o.elem
    if lint.checked then o.editors[_o.input].setOption 'lint', true
    else o.editors[_o.input].setOption 'lint', false
  
  # Runs
  o.runs = 
    input__remark: !-> 
      _html = '<!DOCTYPE html><html><head><style></style></head><body></body></html>' + 
        #'<textarea id="source"></textarea>' +
        '</body></html>'
      _remark = o.editors['input__remark'].getValue!
      previewFrame  = document.getElementById 'output__iframe'
      preview       = previewFrame.contentDocument || previewFrame.contentWindow.document
      previewWindow = previewFrame.contentWindow || previewFrame
      previewWindow.Opal = null
      preview.open!
      preview.write _html
      # HTML (Textarea)
      textarea = preview.createElement 'textarea'
      textarea.id = 'source'
      textarea.innerHTML = o.editors['input__remark'].getValue!
      preview.body .appendChild textarea
      # CSS
      style = preview.createElement 'style'
      style.textContent = dmt['remark-css']
      preview.head .appendChild style
      # JS
      script = preview.createElement 'script'
      script.textContent = dmt['remark-js'] + 
        'var slideshow = remark.create();'
        #'var slideshow = remark.create({ source: window.parent.app.remark()});'
      preview.body .appendChild script
      preview.close!

  # Error
  o.error = (e) !->
    document.getElementById 'output__error' .innerHTML = e
    console.log e
      
  # Import
  o.importer = (input, editor) !->
    reader = new FileReader!
    reader.readAsText input.files[0]
    reader.onload = (e) !->
      o.editors[editor].getDoc! .setValue e.target.result
  
  # Save
  o.save = !->
    for let i, j of o.editors
      localforage.setItem 'rcx/others/remark/' + i, o.editors[i].getValue!, (e, v) -> 
        console.log 'Save ' + i
   
  # Load   
  o.load = !->
    for let i, j of o.editors
      localforage.getItem 'rcx/others/remark/' + i, (e, v) ->
        o.editors[i].getDoc! .setValue v
  
  # Alternative Layout (On/Off)
  o.altLayout = !->
    alt__layout = document.getElementById 'alt__layout'
    _editors = {}
    for i, j of o.editors
      _editors[i] = o.editors[i].getValue!
    if alt__layout.checked then $ '#app__layout' .html dmt['layout-alt']
    else $ '#app__layout' .html dmt['layout-default']
    setTimeout !-> 
      _init!
      for i, j of o.editors
        o.editors[i].getDoc! .setValue _editors[i]
    , 300 
  
  # Init & Return
  _init!
  o

# Defaults
['input__remark'].map (el, i) !-> 
  app.editors[el].getDoc! .setValue dmt[el]