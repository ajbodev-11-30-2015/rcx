
app = do ->
  # Init
  o = 
    editors: {} 
    exports: {}
    runs:    {}
  
  run__auto              = document.getElementById 'run__auto'
  save__auto             = document.getElementById 'save__auto'
  run__delay             = 0
  save__delay            = 0
  
  _init = !->
    # Local Storage
    localforage.setDriver localforage.LOCALSTORAGE
    # Editors
    o.editors = 
      input__src:     _editor id: 'input__src',     mode: 'text/javascript', lint: true
      input__spec:    _editor id: 'input__spec',    mode: 'text/javascript', lint: true
    # Exports
    o.exports =
      input__src:     _exporter input: 'input__src',  type: 'text/javascript;charset=utf-8', ext: 'js'
      input__spec:    _exporter input: 'input__spec', type: 'text/javascript;charset=utf-8', ext: 'js'
      output__iframe: _exporter src: _iframeSrc,      type: 'text/html;charset=utf-8',       ext: 'html'
    # Linters
    o.lints = 
      input__src:   _linter input: 'input__src',  elem: 'lint__input__src'
      input__spec:  _linter input: 'input__spec', elem: 'lint__input__spec'
    # On change, run and/or save
    for let i, j of o.editors
      o.editors[i].on 'change', !->
        clearTimeout run__delay; clearTimeout save__delay
        if run__auto.checked  then run__delay  = setTimeout (!-> o.runs[i]!), 300
        if save__auto.checked then save__delay = setTimeout (!-> o.save!), 300
        console.clear!
  
  # Iframe
  _iframeSrc = ->
    previewFrame = document.getElementById 'output__iframe'
    preview =  previewFrame.contentDocument ||  previewFrame.contentWindow.document;
    '<!DOCTYPE html>' + preview .getElementsByTagName('html')[0] .outerHTML
    
  # Editors
  _editor = (_o) -> 
    CodeMirror.fromTextArea (document.getElementById _o.id), do
      mode: _o.mode
      theme: 'blackboard'
      gutters: ['CodeMirror-lint-markers']
      tabMode: 'indent'
      lineNumbers: true
      indentUnit: 2
      lineWrapping: true
      lint: _o.lint
  
  # Export
  _exporter = (_o) -> !->
    if _o.input then _input = o.editors[_o.input].getValue!
    else _input = _o.src!
    blob = new Blob([_input], type: _o.type)
    date = (moment! .format 'MMM[]Do-h[]mm[]a')
    saveAs blob, 'rcx-testing-qunit-' + date + '.' + _o.ext #'try.html'
  
  # Lint
  _linter = (_o) -> !->
    lint = document.getElementById _o.elem
    if lint.checked then o.editors[_o.input].setOption 'lint', true
    else o.editors[_o.input].setOption 'lint', false
  
  # Runs
  o.runs = 
    input__src: !-> 
      _src  = o.editors['input__src'].getValue!
      _spec = o.editors['input__spec'].getValue!
      _html = 
        '<!DOCTYPE html><html><meta charset="utf-8"><head><style></style></head><body>'
        '<div id="qunit"></div><div id="qunit-fixture"></div>'
        '</body></html>'
      previewFrame  = document.getElementById 'output__iframe'
      preview       = previewFrame.contentDocument || previewFrame.contentWindow.document
      preview.open!
      preview.write _html
      # CSS
      style = preview.createElement 'style'
      style.textContent = dmt['qunit-css']
      preview.head .appendChild style
      # JS
      ['qunit-js'].map (el, i) !->
        script = preview.createElement 'script'
        script.textContent = dmt[el]
        preview.body .appendChild script
      script = preview.createElement 'script'
      script.textContent = 'try {' + _src + '\n' + _spec + '} catch(e) { window.parent.app.error(e); }'
      preview.body .appendChild script
      preview.close!
      /*
      */
    input__spec: !-> o.runs.input__src!
  
  # Error
  o.error = (e) !->
    document.getElementById 'output__error' .innerHTML = e
    console.log e
      
  # Import
  o.importer = (input, editor) !->
    reader = new FileReader!
    reader.readAsText input.files[0]
    reader.onload = (e) !->
      o.editors[editor].getDoc! .setValue e.target.result
  
  # Save
  o.save = !->
    for let i, j of o.editors
      localforage.setItem 'rcx/testing/qunit/' + i, o.editors[i].getValue!, (e, v) -> 
        console.log 'Save ' + i
   
  # Load   
  o.load = !->
    for let i, j of o.editors
      localforage.getItem 'rcx/testing/qunit/' + i, (e, v) ->
        o.editors[i].getDoc! .setValue v
  
  # Alternative Layout (On/Off)
  o.altLayout = !->
    alt__layout = document.getElementById 'alt__layout'
    _editors = {}
    for i, j of o.editors
      _editors[i] = o.editors[i].getValue!
    if alt__layout.checked then $ '#app__layout' .html dmt['layout-alt']
    else $ '#app__layout' .html dmt['layout-default']
    setTimeout !-> 
      _init!
      for i, j of o.editors
        o.editors[i].getDoc! .setValue _editors[i]
    , 300 
  
  # Init & Return
  _init!
  o

# Defaults
['input__src' 'input__spec'].map (el, i) !-> 
  app.editors[el].getDoc! .setValue dmt[el]