
gulp       = require 'gulp'
global.use = require './tasks/use.ls'

jade    = use 'jade'
filters = use 'filters'
  
init  = !->
  jade.filterAll filters # Custom Jade filters; Run BEFORE taskAll
  jade.taskAll build     # gulp compile-<task> | gulp watch-<task>

build = 
  # App : Generates code for server-side frameworks using Jade
  * task: 'app'          src: 'src/app/index.jade'                     dest: 'dist/app/'
  * task: 'app-asp'      src: 'src/app/asp/index.jade'                 dest: 'dist/app/asp/'
  * task: 'app-spring'   src: 'src/app/spring/index.jade'              dest: 'dist/app/spring/'
  * task: 'app-rails'    src: 'src/app/rails/index.jade'               dest: 'dist/app/rails/'
  * task: 'app-laravel'  src: 'src/app/laravel/index.jade'             dest: 'dist/app/laravel/'
  * task: 'app-express'  src: 'src/app/express/index.jade'             dest: 'dist/app/express/'
  * task: 'app-flask'    src: 'src/app/flask/index.jade'               dest: 'dist/app/flask/'
  # Database
  * task: 'sql'          src: 'src/database/sql/index.jade'            dest: 'dist/database/sql/'
  * task: 'nosql'        src: 'src/database/nosql/index.jade'          dest: 'dist/database/nosql/'
  # SPA
  * task: 'angular'      src: 'src/spa/angular/index.jade'             dest: 'dist/spa/angular/'
  * task: 'mithril'      src: 'src/spa/mithril/index.jade'             dest: 'dist/spa/mithril/'
  * task: 'react'        src: 'src/spa/react/index.jade'               dest: 'dist/spa/react/'
  * task: 'ember'        src: 'src/spa/ember/index.jade'               dest: 'dist/spa/ember/'
  * task: 'riot'         src: 'src/spa/riot/index.jade'                dest: 'dist/spa/riot/'
  # Format
  * task: 'format'       src: 'src/format/index.jade'                  dest: 'dist/format/'
  # HTML
  * task: 'html'         src: 'src/html/index.jade'                    dest: 'dist/html/'
  * task: 'html-css-js'  src: 'src/html/html-css-js/index.jade'        dest: 'dist/html/html-css-js/'
  * task: 'jade'         src: 'src/html/jade/index.jade'               dest: 'dist/html/jade/'
  * task: 'markdown'     src: 'src/html/markdown/index.jade'           dest: 'dist/html/markdown/'
  # CSS
  * task: 'css'          src: 'src/css/index.jade'                     dest: 'dist/css/'
  * task: 'stylus'       src: 'src/css/stylus/index.jade'              dest: 'dist/css/stylus/'
  * task: 'less'         src: 'src/css/less/index.jade'                dest: 'dist/css/less/'
  * task: 'sass'         src: 'src/css/sass/index.jade'                dest: 'dist/css/sass/'
  # JavaScript
  * task: 'javascript'   src: 'src/javascript/index.jade'              dest: 'dist/javascript/'
  * task: 'livescript'   src: 'src/javascript/livescript/index.jade'   dest: 'dist/javascript/livescript/'
  * task: 'coffeescript' src: 'src/javascript/coffeescript/index.jade' dest: 'dist/javascript/coffeescript/'
  * task: 'typescript'   src: 'src/javascript/typescript/index.jade'   dest: 'dist/javascript/typescript/'
  * task: 'babel'        src: 'src/javascript/babel/index.jade'        dest: 'dist/javascript/babel/'
  * task: 'react-jsx'    src: 'src/javascript/react-jsx/index.jade'    dest: 'dist/javascript/react-jsx/'
  * task: 'uglifyjs'     src: 'src/javascript/uglifyjs/index.jade'     dest: 'dist/javascript/uglifyjs/'
  # jQuery
  * task: 'jquery'       src: 'src/jquery/index.jade'                  dest: 'dist/jquery/'
  * task: 'datatables'   src: 'src/jquery/datatables/index.jade'       dest: 'dist/jquery/datatables/'
  * task: 'pivottable'   src: 'src/jquery/pivottable/index.jade'       dest: 'dist/jquery/pivottable/'
  # Bootstrap
  * task: 'bootstrap'    src: 'src/bootstrap/index.jade'               dest: 'dist/bootstrap/'
  * task: 'adminlte'     src: 'src/bootstrap/adminlte/index.jade'      dest: 'dist/bootstrap/adminlte/'
  # Testing
  * task: 'jasmine'      src: 'src/testing/jasmine/index.jade'         dest: 'dist/testing/jasmine/'
  * task: 'qunit'        src: 'src/testing/qunit/index.jade'           dest: 'dist/testing/qunit/'
  # D3
  * task: 'd3'           src: 'src/d3/index.jade'                      dest: 'dist/d3/'
  # Language
  * task: 'php'          src: 'src/language/php/index.jade'            dest: 'dist/language/php/'
  * task: 'ruby'         src: 'src/language/ruby/index.jade'           dest: 'dist/language/ruby/'
  * task: 'python'       src: 'src/language/python/index.jade'         dest: 'dist/language/python/'
  # Others
  * task: 'katex'        src: 'src/others/katex/index.jade'            dest: 'dist/others/katex/'
  * task: 'remark'       src: 'src/others/remark/index.jade'           dest: 'dist/others/remark/'
  # __
  ...

init!